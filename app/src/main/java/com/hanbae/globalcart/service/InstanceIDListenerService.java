package com.hanbae.globalcart.service;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import timber.log.Timber;

/**
 * Created by acid on 2016-11-21.
 * 토큰 갱신용 클래스
 */

public class InstanceIDListenerService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        // 이 token을 서버에 전달 한다.

        Log.d("ID", "Refreshed token: " + refreshedToken);
        Log.d("ID", "Refreshed token: " + refreshedToken);
        Log.d("ID", "Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        // 서버에 등록(DB 연동 관련 작업)
        Timber.d("Refresged token: " + token);

    }
}
