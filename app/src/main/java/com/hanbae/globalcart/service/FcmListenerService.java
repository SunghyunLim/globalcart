package com.hanbae.globalcart.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.hanbae.globalcart.R;
import com.hanbae.globalcart.model.ResponsePushMatchingInformation;
import com.hanbae.globalcart.view.activity.checkItem.CheckItemActivity;
import com.hanbae.globalcart.view.activity.main.MainActivity;

import timber.log.Timber;

/**
 * Created by acid on 2016-11-21.
 * 데이터 수신용 클래스
 */

public class FcmListenerService extends FirebaseMessagingService {

    private static final String TAG = "FCM";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage message) {

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + message.getFrom());

        // Check if message contains a data payload.
        if (message.getData().size() > 0) { //도즈모드 풀기 위함
            Log.d(TAG, "Message data payload: " + message.getData());

            if(!message.getData().get("title").equals("title")) {  //Doze모드 푸는건 노티 띄우지 않음

//                Context context = getApplicationContext();
                Intent intent = new Intent(this, CheckItemActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putString("jsonData", message.getData().get("body"));
//                Timber.e("Push Body : "+message.getData().get("body"));
                intent.putExtra("checkItem", message.getData().get("body"));
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(intent);

                PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                Gson gson = new Gson();
                ResponsePushMatchingInformation pushMatchingInformation = gson.fromJson(message.getData().get("body"), ResponsePushMatchingInformation.class);
                Timber.e("pushMatchingInformation : "+pushMatchingInformation);
                Timber.e("pushMatchingInformation : "+intent.getStringExtra("checkItem"));

                try
                {
                    contentIntent.send();
                }
                catch(PendingIntent.CanceledException e)
                {
                    e.printStackTrace();
                }

                sendNotification(message.getData().get("title"), pushMatchingInformation.getMessage(), contentIntent);
            }
        }

        // Check if message contains a notification payload.
//        if (message.getNotification() != null) { // 메세지 받고 노티 띄움
//            Log.d(TAG, "Message Notification Title: " + message.getNotification().getTitle());
//            Log.d(TAG, "Message Notification Body: " + message.getNotification().getBody());
//            if(!title.equals("title")) {  //Doze모드 푸는건 노티 띄우지 않음

//            sendNotification(message.getNotification().getTitle(), message.getNotification().getBody());
//            }
//        }

        // 전달 받은 정보로 뭔가를 하면 된다.
//        if (message.getData().size() > 0) {
//            Log.d("Doze", "도즈모드 풀려고 우선선위 높음으로 받음");
//            Log.d("Message data payload: ", message.getData()+"");
//        }

        Log.d("FCM getFrom : ", message.getFrom());
        Log.d("FCM getMessageId : ", message.getMessageId());
    }

    private void sendNotification(String title, String messageBody, PendingIntent pendingIntent) {
//        Intent intent = new Intent(this, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "gc1")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(ResourcesCompat.getColor(getBaseContext().getResources(), R.color.colorPrimary, null))
                .setLargeIcon(BitmapFactory.decodeResource(getBaseContext().getResources(), R.mipmap.ic_launcher))
                .setTicker("Global Cart")
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setPriority(Notification.PRIORITY_MAX)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody));

        NotificationManagerCompat.from(getBaseContext()).notify(1101, notificationBuilder.build());
    }
}


