package com.hanbae.globalcart.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by acid on 2016-11-02.
 */
//         @Retention(RetentionPolicy.RUNTIME) : VM에 의해 유지되고, runtime에만 읽을 수 있음.
//
//         @Retention(RetentionPolicy.CLASS) : compiler에 의해서 compile time에 유지됨.
//
//         @Retention(RetentionPolicy.SOURCE) : source level에서 유지되며, compiler에서는 무시됨.


@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface FragmentScope {
}
