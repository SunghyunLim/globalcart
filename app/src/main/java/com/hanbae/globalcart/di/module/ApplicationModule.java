package com.hanbae.globalcart.di.module;


import com.google.gson.Gson;
import com.hanbae.globalcart.BaseApplication;
import com.hanbae.globalcart.data.remote.APIService;
import com.hanbae.globalcart.util.PreferenceUtil;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/*
* 이 모듈은 어플리케이션 생명주기동안 살아있는 객체들을 제공
* 즉, @Provide 가 달린 모든 메서드에 @Singleton 스코프를 사용하는 이유
* */
@Module
public class  ApplicationModule {

    private final BaseApplication mBaseApplication;

    public ApplicationModule(BaseApplication baseApplication) {
        this.mBaseApplication = baseApplication;
    }

    @Provides
    @Singleton
    public BaseApplication provideApplication() {return mBaseApplication; }

    @Provides
    @Singleton
    public APIService provideApiService() { return APIService.Factory.create(mBaseApplication); }

    @Provides
    @Singleton
    public Gson gson() { return new Gson(); }

    @Provides
    @Singleton
    public PreferenceUtil providePreferenceUtil() {return new PreferenceUtil(mBaseApplication, "notEncrypt");}

}