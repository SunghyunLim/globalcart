package com.hanbae.globalcart.di.component;

import com.hanbae.globalcart.di.ActivityScope;
import com.hanbae.globalcart.di.module.ActivityModule;
import com.hanbae.globalcart.view.base.AppBaseActivity;

import dagger.Component;

@ActivityScope //액티비티의 생명주기 동안 살아 있는 컴포넌트
/*
* 이렇게 스코프를 정해서 사용할 때 장점은
* 액티비티가 생성되어있길 요구하는 부분에 객체 인젝트
* 액티비티 기반의 싱글톤 사용
* 액티비티에서만 사용하는 것을 글로벌 객체 그래프와 분리
* */

@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent extends ApplicationComponent {

    void inject(AppBaseActivity appBaseActivity);

//    void inject(AppBaseActivity appBaseActivity);
//
//    void inject(MainActivity mainActivity);


}