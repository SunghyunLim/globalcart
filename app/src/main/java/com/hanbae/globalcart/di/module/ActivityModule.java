package com.hanbae.globalcart.di.module;

import android.app.Activity;
import android.content.Context;

import com.hanbae.globalcart.di.ActivityScope;
import com.hanbae.globalcart.view.base.AppBaseFragment;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;

@Module
/*
* 이 모듈은 그래프의 자손들에게 액티비티를 제공
* 예를 들어 프래그먼트에서 액티비티 컨텍스트를 사용.
* */
public class ActivityModule {
    private final Activity mActivity;

    public List<Class<? extends AppBaseFragment>> mBeforeFragment;

    public ActivityModule(Activity activity) {
        mActivity = activity;
        mBeforeFragment = new ArrayList<Class<? extends AppBaseFragment>>();
    }

    @Provides
    @ActivityScope
    public Context activityContext() {
        return mActivity;
    }

    @Provides
    @ActivityScope
    public List<Class<? extends AppBaseFragment>> getBeforeFragment() {
        return mBeforeFragment;
    }

}