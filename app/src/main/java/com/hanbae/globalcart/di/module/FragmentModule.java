package com.hanbae.globalcart.di.module;

import android.support.v4.app.Fragment;
import com.hanbae.globalcart.di.FragmentScope;
import dagger.Module;
import dagger.Provides;

@Module
public class FragmentModule {
    private final Fragment mFragment;

    public FragmentModule(Fragment fragment) {
        mFragment = fragment;
    }

    @Provides
    @FragmentScope
    Fragment fragment(){
        return mFragment;
    }

}