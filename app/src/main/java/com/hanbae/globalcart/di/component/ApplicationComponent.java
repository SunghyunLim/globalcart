package com.hanbae.globalcart.di.component;

import android.content.Context;

import com.google.gson.Gson;
import com.hanbae.globalcart.BaseApplication;
import com.hanbae.globalcart.data.remote.APIService;
import com.hanbae.globalcart.data.remote.CustomHttpLoggingInterceptor;
import com.hanbae.globalcart.data.remote.NetworkErrorDetectInterceptor;
import com.hanbae.globalcart.di.module.ApplicationModule;
import com.hanbae.globalcart.util.PreferenceUtil;
import com.hanbae.globalcart.view.activity.checkItem.CheckItemPresenter;
import com.hanbae.globalcart.view.activity.login.LoginPresenter;
import com.hanbae.globalcart.view.activity.main.MainPresenter;
import com.hanbae.globalcart.view.base.AppBaseActivity;
import com.hanbae.globalcart.view.base.AppBaseFragment;
import com.hanbae.globalcart.view.fragment.history.BuyHistoryPresenter;
import com.hanbae.globalcart.view.fragment.history.SellHistoryPresenter;
import com.hanbae.globalcart.view.fragment.home.BuyFragment;
import com.hanbae.globalcart.view.fragment.home.BuyPresenter;
import com.hanbae.globalcart.view.fragment.home.SellPresenter;

import javax.inject.Singleton;
import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

//    ActivityComponent addActivityComponent(ActivityModoule activityModoule);

    // DI 대상 선언 : 메소드명은 의미 없고 참조변수가 실제 DI 대상
    void inject(BaseApplication baseApplication);
    void inject(AppBaseFragment appBaseFragment);

    // Network 관련
    void inject(CustomHttpLoggingInterceptor customHttpLoggingInterceptor);
    void inject(NetworkErrorDetectInterceptor networkErrorDetectInterceptor);

    //Login
    void inject(LoginPresenter loginPresenter);
    void inject(MainPresenter loginPresenter);

    //Home
    void inject(BuyPresenter buyPresenter);
    void inject(SellPresenter sellPresenter);

    //CheckItem
    void inject(CheckItemPresenter checkItemPresenter);

    //History
    void inject(BuyHistoryPresenter buyHistoryPresenter);
    void inject(SellHistoryPresenter sellHistoryPresenter);

//
//    void inject(UserInformationPresenter userInformationPresenter);

    //Exposed to sub-graphs.
    //하위 그래프에 아래 클래스들을 공개
    APIService apiService();

    Gson gson();

    PreferenceUtil priPreferenceUtil();

}