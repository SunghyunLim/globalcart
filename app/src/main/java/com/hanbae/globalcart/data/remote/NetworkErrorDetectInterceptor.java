package com.hanbae.globalcart.data.remote;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.google.gson.Gson;
import com.hanbae.globalcart.BaseApplication;
import com.hanbae.globalcart.util.NetworkUtil;
import java.io.IOException;
import javax.inject.Inject;
import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class NetworkErrorDetectInterceptor implements Interceptor {

    private Context context;

    @Inject
    Gson gson;

    public NetworkErrorDetectInterceptor(Context context) {
        BaseApplication.get(context).getApplicationComponent().inject(this);
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        if(!NetworkUtil.isNetworkConnected(context)){
//            eventBus.post(new MessagesEvent(false, context.getString(R.string.network_error_text), 0));
        }

        Timber.i("NetworkErrorDetectInterceptor request================================");
        Response response = chain.proceed(chain.request());
        Response newResponse = null;
        Timber.i("NetworkErrorDetectInterceptor response================================");
        if (response.code() != 200) {
            Timber.e("Response code is not 200~!");
//            new Handler(Looper.getMainLooper()).post(() -> eventBus.post(new MessagesEvent(true, context.getString(R.string.network_error_text), response.code())));
            return response;
        }else{
//            String resonseBodyStr = AmhohwaInterceptor.responseBodyToString(response.body());
//            Timber.i("resonseBody String : " + resonseBodyStr);
//            CommonHaribo commonHaribo = gson.fromJson(resonseBodyStr, CommonHaribo.class);

//            if(commonHaribo.getServiceCode().equals("101")){
//                new Handler(Looper.getMainLooper()).post(() -> eventBus.post(new MessagesEvent(true, context.getString(R.string.server_error_text), 101)));
//            }
//            if(commonHaribo.getServiceCode().equals("102")){
//                new Handler(Looper.getMainLooper()).post(() -> eventBus.post(new MessagesEvent(true, context.getString(R.string.db_error_text), 102)));
//            }
//            ResponseBody responseBody = ResponseBody.create(response.body().contentType(), resonseBodyStr);
//            newResponse = new Response.Builder().code(response.code()).headers(response.headers()
//            ).body(responseBody).request(response.request()).protocol(response.protocol()).message(response.message()).networkResponse(response.networkResponse()).build();
            Timber.i("resonseBody String 나가기");
        }
        return response;
    }
}