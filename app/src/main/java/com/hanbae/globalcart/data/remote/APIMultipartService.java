package com.hanbae.globalcart.data.remote;

import android.content.Context;

import com.hanbae.globalcart.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;
import timber.log.Timber;

public interface APIMultipartService {

//    public static final String IMAGE_URL = "http://122.199.225.68:1983/havit/uploadFile/";
    public static final String IMAGE_URL = "http://13.124.42.235:1983/havit/uploadFile/";

//    String ENDPOINT = "http://122.199.225.68:1983/havit/mobile/"; // retrofit2부터 url뒤에 /를 입력해야 함 테스트서버
    String ENDPOINT = "http://59.10.164.85:1321/"; // 실서버
    String IMAGE_PATH = "bitcare/HealthContent/";
    String AMHOHWA_KEY = "BitCare_Ver2.0_Crypto";

    boolean amhohwa = true;

    // 파일 저장
    @POST("item/insertFile.do")
    @Multipart
    Observable<Response<ResponseBody>> addFile(@Part MultipartBody.Part file);

    class Factory {

        public static APIMultipartService create(Context context) {

            OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
            builder.readTimeout(30, TimeUnit.SECONDS);
            builder.connectTimeout(20, TimeUnit.SECONDS);
            builder.writeTimeout(60, TimeUnit.SECONDS);

            if (BuildConfig.DEBUG) {
                CustomHttpLoggingInterceptor interceptor = new CustomHttpLoggingInterceptor();
                interceptor.setLevel(CustomHttpLoggingInterceptor.Level.BASIC);
                builder.addInterceptor(interceptor);
            }

            builder.addInterceptor(new NetworkErrorDetectInterceptor(context));

//            if (amhohwa) {
//                builder.addInterceptor(new AmhohwaInterceptor2());
//                Timber.e("암호화 적용!");
//            }

            //Extra Headers
//            builder.addNetworkInte
// rceptor(chain -> {
//                Request request = chain.request().newBuilder().addHeader("Authorization", authToken).build();
//                return chain.proceed(request);
//            });

            int cacheSize = 10 * 1024 * 1024; // 10 MiB
            Cache cache = new Cache(context.getCacheDir(), cacheSize);
            builder.cache(cache);

            OkHttpClient client = builder.build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(APIMultipartService.ENDPOINT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
//                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();

            return retrofit.create(APIMultipartService.class);

        }
    }
}