package com.hanbae.globalcart.data.remote;

import android.content.Context;

import com.hanbae.globalcart.BuildConfig;
import com.hanbae.globalcart.model.RequestInsertItem;
import com.hanbae.globalcart.model.RequestInsertSearch;
import com.hanbae.globalcart.model.RequestIsRegist;
import com.hanbae.globalcart.model.RequestLogin;
import com.hanbae.globalcart.model.RequestRegist;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

public interface APIService {

//    public static final String IMAGE_URL = "http://59.10.164.85:1321/uploadFile/test/";
    public static final String IMAGE_URL = "http://59.10.164.85:1321/item/getImage.do?param=";



//    String ENDPOINT = "http://122.199.225.68:1983/"; // retrofit2부터 url뒤에 /를 입력해야 함 테스트서버
    String ENDPOINT = "http://59.10.164.85:1321/"; // 실서버
    String IMAGE_PATH = "bitcare/HealthContent/";
//    String AMHOHWA_KEY = "BitCare_Ver2.0_Crypto";

    boolean amhohwa = false;

    //UID 존재 여부 확인
    @POST("user/isRegist.do")
    @Headers({"Content-Type: application/json"})
    Observable<Response<ResponseBody>> isRegist(@Body RequestIsRegist requestIsRegist);

    //로그인
    @POST("user/login.do")
    @Headers({"Content-Type: application/json"})
    Observable<Response<ResponseBody>> login(@Body RequestLogin requestLogin);

    //사용자 등록
    @POST("user/regist.do")
    @Headers({"Content-Type: application/json"})
    Observable<Response<ResponseBody>> regist(@Body RequestRegist requestRegist);

    //구매 물품 등록
    @POST("search/insertSearch.do")
    @Headers({"Content-Type: application/json"})
    Observable<Response<ResponseBody>> insertSearch(@Body RequestInsertSearch requestInsertSearch);

    //판매 물품 등록
    @POST("item/insertItem.do")
    @Headers({"Content-Type: application/json"})
    Observable<Response<ResponseBody>> insertItem(@Body RequestInsertItem requestInsertItem);

    ////////////////////////////////////////////////////////////////////////////////////////
    //서버 접속 가능 여부 확인
    @POST("/connection/connection.do")
    @FormUrlEncoded
    Observable<Response<ResponseBody>> checkConnection(@FieldMap Map<String, String> param);

    //사용자정보(부가정보) 수정(업데이트) 없으면 insert 있으면 update 됨
    @POST("bitcare/mobile/setting/updateUserInfo.do")
    @FormUrlEncoded
    Observable<Response<ResponseBody>> updateUserInfo(@FieldMap Map<String, String> param);

    //사용자 목표정보 저장
    @POST("bitcare/mobile/setting/addMeasureGoalInfo.do")
    @FormUrlEncoded
    Observable<Response<ResponseBody>> addGoalInfo(@FieldMap Map<String, String> param);

    //메인데이터 받아오기
    @POST("bitcare/mobile/main/main.do")
    @FormUrlEncoded
    Observable<Response<ResponseBody>> getMainData(@FieldMap Map<String, String> param);

    //목표 설정 받아오기
    @POST("bitcare/mobile/setting/measureGoalInfo.do")
    @FormUrlEncoded
    Observable<Response<ResponseBody>> measureGoalInfo(@FieldMap Map<String, String> param);

    @GET("/addt/addtTempList")
    Observable<Response<ResponseBody>> addtTempList(
            @Query("token") String token,
            @Query("wrtusernm") String wrtusernm,
            @Query("wrtresid") String wrtresid,
            @Query("type") String type
    );

    //의향서 임시 저장
//    @Headers({"Content-Type: application/json"})
//    @POST("/addt/save/addtTemp")
//    Observable<Response<ResponseBody>> addtTempInfo(@Body BasicAddtTempInfoVO requestAddtTemp, @Query("token") String token);

    // 파일 저장
    @POST("daily/addFile.do")
    @Multipart
    Observable<Response<ResponseBody>> addFile(@Part MultipartBody.Part file);

    class Factory {

        public static APIService create(Context context) {
            OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
            builder.readTimeout(30, TimeUnit.SECONDS);
            builder.connectTimeout(20, TimeUnit.SECONDS);
            builder.writeTimeout(60, TimeUnit.SECONDS);

            if (BuildConfig.DEBUG) {
//                builder.addNetworkInterceptor(new StethoInterceptor());
                CustomHttpLoggingInterceptor interceptor = new CustomHttpLoggingInterceptor();
                interceptor.setLevel(CustomHttpLoggingInterceptor.Level.BASIC);
                builder.addInterceptor(interceptor);
            }

            builder.addInterceptor(new NetworkErrorDetectInterceptor(context));

            int cacheSize = 10 * 1024 * 1024; // 10 MiB
            Cache cache = new Cache(context.getCacheDir(), cacheSize);
            builder.cache(cache);

            OkHttpClient client = builder.build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(APIService.ENDPOINT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();

            return retrofit.create(APIService.class);

        }
    }
}