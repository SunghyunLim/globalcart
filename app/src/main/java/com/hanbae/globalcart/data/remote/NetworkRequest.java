package com.hanbae.globalcart.data.remote;

import android.os.Handler;
import android.os.Looper;


import rx.Observable;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import timber.log.Timber;

/**
 * Created by acid on 2016-11-30.
 */

public class NetworkRequest {

    private static final String TAG = "NetworkRequest";

    // Default error handling
    private static Action1<Throwable> mOnError = new Action1<Throwable>() {
        @Override
        public void call(Throwable throwable) {
            Timber.e(throwable.getMessage());
            throwable.printStackTrace();

//            new Handler(Looper.getMainLooper()).post(() -> myEventBus.post(new MessagesEvent(true, BaseApplication.getGlobalApplicationContext().getString(R.string.server_error_text), 100)));
        }
    };

    public static <T> Subscription performAsyncRequest(Observable<T> observable, Action1<? super T> onAction) {
        // Use default error handling
        return performAsyncRequest(observable, onAction, mOnError);
    }

    public static <T> Subscription performAsyncRequest(Observable<T> observable, Action1<? super T> onAction, Action1<Throwable> onError) {
        // Specify a scheduler (Scheduler.newThread(), Scheduler.immediate(), ...)
        // We choose Scheduler.io() to perform network request in a thread pool
        return observable.subscribeOn(Schedulers.io())
                // Observe result in the main thread to be able to update UI
                .observeOn(AndroidSchedulers.mainThread())
                // Set callbacks actions
                .subscribe(onAction, onError); //에러도 던지긴 하는데 이미 앞에서 다 Check해서 다른곳으로 던지기 때문에 실제 구현단에서는 사용 못함
    }

}