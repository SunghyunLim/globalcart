package com.hanbae.globalcart.model;

import lombok.Data;

@Data
public class ResponseAddFile {
    private Result result;
    private String serviceMsg;
    private int serviceCode;
    @Data
    public static class Result {
        private FileUploadResultVO fileUploadResultVO;
        private ResponseAddFile.Result.InResult result;

        @Data
        public static class InResult {
            private String resultCode;
            private String resultMsg;
        }

        @Data
        public static class FileUploadResultVO {
            private String fileName;
            private String fileSerialID;
        }
    }

}
