package com.hanbae.globalcart.model;

import lombok.Data;

@Data
public class ResponseResult {
    String serviceCode;
    String serviceMsg;
    Result result;

    @Data
    public static class Result {
        private String resultCode;
        private String resultMsg;
    }
}
