package com.hanbae.globalcart.model;

import java.util.List;

import lombok.Data;

@Data
public class ResponseIsRegist {
    String serviceCode;
    String serviceMsg;
    Result result;

    @Data
    public static class Result {
        private String resultCode;
        private String resultMsg;
    }
}
