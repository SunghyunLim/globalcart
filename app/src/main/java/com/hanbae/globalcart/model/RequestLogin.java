package com.hanbae.globalcart.model;

import lombok.Data;

@Data
public class RequestLogin {
    String uid;
    String userCelRegistId;
    String userCelSid;
    String userCelTyp;
}
