package com.hanbae.globalcart.model;

import lombok.Data;

@Data
public class RequestInsertSearch {
    String buyUserSerialID;
    String searchText;
    String searchPrice;
    String searchNewYN;
}
