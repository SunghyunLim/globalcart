package com.hanbae.globalcart.model;

import java.util.List;

import lombok.Data;

@Data
public class ResponsePushMatchingInformation {
    private JsonData data;
    private String message;

    @Data
    public static class JsonData{
        private String itemColor;
        private String sellUserSerialID;
        private String itemNewYN;
        private String buyUserSerialID;
        private String itemSerialID;
        private String sellUserProfileURL;
        private String buyUserCellPhone;
        private String sellUserCellPhone;
        private String matchRegistDatetime;
        private String matchCompleteDatetime;
        private String itemName;
        private String searchSerialID;
        private String buyUserName;
        private String matchSerialID;
        private String itemSid;
        private String buyUserProfileURL;
        private String matchSuccessType;
        private String itemComment;
        private String sellUserName;
        private int itemPrice;
        private List<ItemImageList> itemImageList;

        @Data
        public static class ItemImageList {
            private String fileName;
            private String fileSerialID;
        }
    }
}
