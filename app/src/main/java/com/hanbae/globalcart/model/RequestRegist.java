package com.hanbae.globalcart.model;

import lombok.Data;

@Data
public class RequestRegist {
    String uid;
    String userCelRegistId;
    String userCelSid;
    String userCelType;
    String userCellPhone;
    String userName;
}
