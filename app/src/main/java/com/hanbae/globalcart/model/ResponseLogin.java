package com.hanbae.globalcart.model;

import lombok.Data;

@Data
public class ResponseLogin {
    String serviceCode;
    String serviceMsg;
    Result result;

    @Data
    public static class Result {
        private String userSerialID;
        private String userName;
        private String userCellPhone;
        private String userRegistDateTime;
    }
}
