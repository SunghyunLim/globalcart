package com.hanbae.globalcart.model;

import java.util.List;
import lombok.Data;

@Data
public class RequestInsertItem {
    private String itemColor;
    private String itemComment;
    private String itemName;
    private String itemNewYN;
    private String itemSerialID;
    private String itemSid;
    private String sellUserSerialID;
    private int itemPrice;
    private List<ItemImageList> itemImageList;

    @Data
    public static class ItemImageList {
        private String fileName;
        private String fileSerialID;
    }

}
