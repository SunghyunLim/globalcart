package com.hanbae.globalcart;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.VisibleForTesting;

import com.hanbae.globalcart.di.component.ApplicationComponent;
import com.hanbae.globalcart.di.component.DaggerApplicationComponent;
import com.hanbae.globalcart.di.module.ApplicationModule;

import rx.Scheduler;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by acid on 2018-03-20.
 */

public class BaseApplication extends Application {
    private static volatile BaseApplication instance = null;
    private Scheduler mScheduler;
    private ApplicationComponent mApplicationComponent;

//    public Scheduler getSubscribeScheduler() {
//        if (mScheduler == null) {
//            mScheduler = Schedulers.io();
//        }
//        return mScheduler;
//    }

    /**
     * singleton 애플리케이션 객체를 얻는다.
     * @return singleton 애플리케이션 객체
     */
    public static BaseApplication getGlobalApplicationContext() {
        if(instance == null)
            throw new IllegalStateException("this application does not inherit BaseApplication");
        return instance;
    }

    public static BaseApplication get(Context context) {
        return (BaseApplication) context.getApplicationContext();
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }

    @VisibleForTesting
    public void setApplicationComponent(ApplicationComponent applicationComponent) {
        this.mApplicationComponent = applicationComponent;
    }

    public rx.Scheduler getSubscribeScheduler() {
        if (mScheduler == null) {
            mScheduler = Schedulers.io();
        }
        return mScheduler;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        boolean isDebuggable = (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)); //Debug 여부

        if (isDebuggable) {
            Timber.plant(new Timber.DebugTree());
        }

        mApplicationComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
        mApplicationComponent.inject(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            android.app.NotificationManager notificationManager = (android.app.NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channelMessage = new NotificationChannel("gc1", "globalcart", android.app.NotificationManager.IMPORTANCE_DEFAULT);
            channelMessage.setDescription("channel description"); channelMessage.enableLights(true);
            channelMessage.setLightColor(Color.GREEN); channelMessage.enableVibration(true);
            channelMessage.setVibrationPattern(new long[]{100, 200, 100, 200});
            channelMessage.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            notificationManager.createNotificationChannel(channelMessage); }


            // 그룹 지정
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            android.app.NotificationManager notificationManager = (android.app.NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            NotificationChannelGroup group1 = new NotificationChannelGroup("channel_group_id", "channel_group_name");
//            notificationManager.createNotificationChannelGroup(group1);
//            NotificationChannel channelMessage = new NotificationChannel("channel_id", "channel_name", android.app.NotificationManager.IMPORTANCE_DEFAULT);
//            channelMessage.setDescription("channel description"); channelMessage.setGroup("channel_group_id");
//            channelMessage.enableLights(true); channelMessage.setLightColor(Color.GREEN);
//            channelMessage.enableVibration(true);
//            channelMessage.setVibrationPattern(new long[]{100, 200, 100, 200});
//            channelMessage.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
//            notificationManager.createNotificationChannel(channelMessage); }
    }
}
