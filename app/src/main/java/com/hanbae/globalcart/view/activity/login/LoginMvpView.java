package com.hanbae.globalcart.view.activity.login;

import com.hanbae.globalcart.view.base.AppBaseMvpView;

/**
 * Created by acid on 2016-11-04.
 */

public interface LoginMvpView extends AppBaseMvpView {
    public void enterMainActivity();
}
