package com.hanbae.globalcart.view.activity.login;


import android.content.Context;
import android.provider.Settings;
import android.widget.Toast;

import com.bit.bitcare.baseapp.presenter.BasePresenter;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.hanbae.globalcart.BaseApplication;
import com.hanbae.globalcart.data.remote.APIService;
import com.hanbae.globalcart.data.remote.NetworkRequest;
import com.hanbae.globalcart.model.RequestIsRegist;
import com.hanbae.globalcart.model.RequestLogin;
import com.hanbae.globalcart.model.RequestRegist;
import com.hanbae.globalcart.model.ResponseIsRegist;
import com.hanbae.globalcart.model.ResponseLogin;
import com.hanbae.globalcart.model.ResponseRegist;
import com.hanbae.globalcart.util.PreferenceUtil;

import javax.inject.Inject;
import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Subscription;
import timber.log.Timber;

public class LoginPresenter implements BasePresenter<LoginMvpView> {

    private Context context;
    private LoginMvpView loginMvpView;
    private Subscription mSubscription;
    private String returnString;
    private String userIDType;

    @Inject
    public LoginPresenter(Context context) {
        ((BaseApplication) context.getApplicationContext()).getApplicationComponent().inject(this);
        this.context = context;
    }

    @Inject
    Gson gson;

    @Inject
    PreferenceUtil preferenceUtil;

    @Inject
    APIService apiService;

    @Override
    public void attachView(LoginMvpView view) {
        loginMvpView = view;
    }

    public Gson getGson (){
        return gson;
    }

    @Override
    public void detachView() {
        loginMvpView = null;
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    public PreferenceUtil getPreferenceUtil() {
        return preferenceUtil;
    }

    // UID 존재 여부 확인
    public void isRegist(String uid, String name) {
        RequestIsRegist requestIsRegist = new RequestIsRegist();
        requestIsRegist.setUid(uid);
        Timber.e("requestIsRegist : " + requestIsRegist);

        //사용자 정보 등록 했는지 확인 후 사용자 정보 추가 혹은 사용자 정보 가져오기 실행
        if (mSubscription != null) mSubscription.unsubscribe();
        mSubscription = NetworkRequest.performAsyncRequest(apiService.isRegist(requestIsRegist), (data) -> {
            try {
                returnString = new String(((Response<ResponseBody>) data).body().bytes());
                Timber.e("returnString : " + returnString);
                ResponseIsRegist response = gson.fromJson(returnString, ResponseIsRegist.class);
                Timber.d("응답받은 값 : " + returnString);
                Timber.d("응답받은 값(obj) : " + response.toString());

                if("0".equals(response.getResult().getResultCode())){//일치하는게 없음 = 등록이 되어있지 않음
                    regist(uid, name);
                }else{//등록 되어 있음
                    login(uid);
                }
//                Timber.d("gson.toJson(haribo.getResult()) : " + gson.toJson(response.get()));
//                preferenceUtil.putStringExtra("userAccountData", gson.toJson(haribo.getResult()));
//                getLogin(uid, userIDType);
            } catch (Exception e) {
                Timber.e("Exception : " + e.getMessage());
                FirebaseCrash.report(e);
            } finally {

            }
        }, (error) -> {
        });
    }

    // 로그인 후 정보 받음
    public void login(String uid) {
        RequestLogin requestLogin = new RequestLogin();
        requestLogin.setUid(uid);
        requestLogin.setUserCelTyp("A");
        requestLogin.setUserCelSid(Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
        requestLogin.setUserCelRegistId(FirebaseInstanceId.getInstance().getToken());

        Timber.e("requestLogin : " + requestLogin);
        //사용자 정보 등록 했는지 확인 후 사용자 정보 추가 혹은 사용자 정보 가져오기 실행
        if (mSubscription != null) mSubscription.unsubscribe();
        mSubscription = NetworkRequest.performAsyncRequest(apiService.login(requestLogin), (data) -> {
            try {
                returnString = new String(((Response<ResponseBody>) data).body().bytes());
                Timber.e("returnString : " + returnString);
                ResponseLogin response = gson.fromJson(returnString, ResponseLogin.class);
                Timber.d("응답받은 값 : " + returnString);
                Timber.d("응답받은 값(obj) : " + response.toString());

                preferenceUtil.putStringExtra("userInfo", returnString);
                preferenceUtil.putStringExtra("userName", response.getResult().getUserName());
                preferenceUtil.putStringExtra("userSID", response.getResult().getUserSerialID());
//                preferenceUtil.putStringExtra("userProfile", response.getResult().get);

                loginMvpView.enterMainActivity();

            } catch (Exception e) {
                Timber.e("Exception : " + e.getMessage());
                FirebaseCrash.report(e);
            }finally {

            }
        }, (error) -> {
        });
    }

    // 사용자 데이터 등록
    public void regist(String uid, String name) {
        RequestRegist requestRegist = new RequestRegist();
        requestRegist.setUid(uid);
        requestRegist.setUserName(name);
        requestRegist.setUserCelType("A");
        requestRegist.setUserCellPhone("01034861234");
        requestRegist.setUserCelSid(Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
        requestRegist.setUserCelRegistId(FirebaseInstanceId.getInstance().getToken());

        Timber.e(requestRegist.toString());
        //사용자 정보 등록 했는지 확인 후 사용자 정보 추가 혹은 사용자 정보 가져오기 실행
        if (mSubscription != null) mSubscription.unsubscribe();
        mSubscription = NetworkRequest.performAsyncRequest(apiService.regist(requestRegist), (data) -> {
            try {
                returnString = new String(((Response<ResponseBody>) data).body().bytes());
                Timber.e("returnString : " + returnString);
                ResponseRegist response = gson.fromJson(returnString, ResponseRegist.class);
                Timber.d("응답받은 값 : " + returnString);
                Timber.d("응답받은 값(obj) : " + response.toString());
                if(response.getResult()!=null){
                    login(uid);
                }else{
                    Toast.makeText(context,"회원정보 등록에 실패했습니다.", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Timber.e("Exception : " + e.getMessage());
                FirebaseCrash.report(e);
            }finally {

            }
        }, (error) -> {
        });
    }
}