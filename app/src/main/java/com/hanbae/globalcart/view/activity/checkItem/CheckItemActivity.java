package com.hanbae.globalcart.view.activity.checkItem;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.hanbae.globalcart.R;
import com.hanbae.globalcart.data.remote.APIService;
import com.hanbae.globalcart.model.ResponseLogin;
import com.hanbae.globalcart.model.ResponsePushMatchingInformation;
import com.hanbae.globalcart.util.BackButtonPressCloseHandler;
import com.hanbae.globalcart.view.activity.login.LoginActivity;
import com.hanbae.globalcart.view.activity.main.MainMvpView;
import com.hanbae.globalcart.view.activity.main.MainPresenter;
import com.hanbae.globalcart.view.base.AppBaseActivity;
import com.hanbae.globalcart.view.fragment.home.BuyFragment;
import com.hanbae.globalcart.view.fragment.home.SellFragment;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class CheckItemActivity extends AppBaseActivity implements CheckItemMvpView {

    private CheckItemPresenter checkItemPresenter;

    @BindView(R.id.tv_check_item_title) TextView tv_check_item_title; //툴바 타이틀
    @BindView(R.id.tv_check_item_title_user_name) TextView tv_check_item_title_user_name; //사용자명 타이틀
    @BindView(R.id.tv_check_item_content_user_name) TextView tv_check_item_content_user_name; //사용자(구매자 혹은 판매자)명
    @BindView(R.id.tv_check_item_title_name) TextView tv_check_item_title_name; //물품명 타이틀
    @BindView(R.id.tv_check_item_content_name) TextView tv_check_item_content_name; //물품명
    @BindView(R.id.tv_check_item_title_price) TextView tv_check_item_title_price; //물품 가격 타이틀
    @BindView(R.id.tv_check_item_content_price) TextView tv_check_item_content_price; //물품 가격
    @BindView(R.id.tv_check_item_title_type) TextView tv_check_item_title_type; //물품 구분 타이틀
    @BindView(R.id.tv_check_item_content_type) TextView tv_check_item_content_type; //물품 구분(신품 혹은 중고)
    @BindView(R.id.tv_check_item_title_color) TextView tv_check_item_title_color; //물품 색상 타이틀
    @BindView(R.id.tv_check_item_content_color) TextView tv_check_item_content_color; //물품 색상
    @BindView(R.id.tv_check_item_title_comment) TextView tv_check_item_title_comment; //물품 설명 타이틀
    @BindView(R.id.tv_check_item_content_comment) TextView tv_check_item_content_comment; //물품 설명
    @BindView(R.id.tv_check_item_title_image) TextView tv_check_item_title_image; //물품 이미지 타이틀
    @BindView(R.id.iv_check_item_content_image) ImageView iv_check_item_content_image; //물품 이미지

    @OnClick({R.id.btn_check_item, R.id.ll_check_item_close, R.id.iv_check_item_close})
    public void btnClick(View v) {
        switch(v.getId()){
            case R.id.btn_check_item:
                Toast.makeText(getContext(),"물품 확인을 하셨습니다.",Toast.LENGTH_SHORT).show();
                finish();
                //물품 확인 프로세스가 필요한가? 구매자는 여기서 바로 구매하는것이 좋을듯
                //만약 x버튼을 누르거나 back키를 눌러 취소한 경우에는 유예 시간을 두어 히스토리로 가서 몇분 동안은 구매 가능하도록...
                break;
            case R.id.ll_check_item_close:
            case R.id.iv_check_item_close:
                Toast.makeText(getContext(),"물품 확인을 취소 하셨습니다.",Toast.LENGTH_SHORT).show();
                finish();
                break;
        }
    }
    @Override
    protected int getResourceLayout() {
        return R.layout.activity_check_item;
    }


    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        checkItemPresenter = new CheckItemPresenter(this);
        checkItemPresenter.attachView(this);

        Bundle extras = getIntent().getExtras(); if (extras == null) { CharSequence  s = "error"; }

        String jsonData = getIntent().getStringExtra("checkItem");
        Timber.e(jsonData);

        ResponsePushMatchingInformation pushMatchingInformation = checkItemPresenter.getGson().fromJson(jsonData, ResponsePushMatchingInformation.class);

//        check_item_text.setText(pushMatchingInformation.toString());

        String userSID = getPreferenceUtil().getStringExtra("userSID");
//        ResponseLogin responseLogin = gson.fromJson(userInfo, ResponseLogin.class);
//        user_name.setText(responseLogin.getResult().getUserName());

        if(userSID.equals(pushMatchingInformation.getData().getBuyUserSerialID())){//내가 구매자 아이디랑 같을 때
            tv_check_item_content_user_name.setText(pushMatchingInformation.getData().getSellUserName());
        }else{
            tv_check_item_title_user_name.setText("구매자명");
            tv_check_item_content_user_name.setText(pushMatchingInformation.getData().getBuyUserName());
            tv_check_item_title.setText("판매 물품 확인");
        }


        String newYN = "신품";
        if("N".equals(pushMatchingInformation.getData().getItemNewYN())){
            newYN = "중고";
        }

        tv_check_item_content_name.setText(pushMatchingInformation.getData().getItemName());
        tv_check_item_content_price.setText(String.format("%,d", pushMatchingInformation.getData().getItemPrice())+"원");
        tv_check_item_content_type.setText(newYN);
        if("".equals(pushMatchingInformation.getData().getItemColor())){
            tv_check_item_content_color.setVisibility(View.GONE);
            tv_check_item_title_color.setVisibility(View.GONE);
        }else{
            tv_check_item_content_color.setText(pushMatchingInformation.getData().getItemColor());
        }
        if("".equals(pushMatchingInformation.getData().getItemComment())){
            tv_check_item_title_comment.setVisibility(View.GONE);
            tv_check_item_content_comment.setVisibility(View.GONE);
        }else{
            tv_check_item_content_comment.setText(pushMatchingInformation.getData().getItemComment());
        }

        Glide.with(mContext).load(APIService.IMAGE_URL+pushMatchingInformation.getData().getItemImageList().get(0).getFileName()).into(iv_check_item_content_image);

    }

    @Override
    public void onStart() {
        super.onStart();

    }


    @Override
    protected void onResume() {
        super.onResume();
    }


}
