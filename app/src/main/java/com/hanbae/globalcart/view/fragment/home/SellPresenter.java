package com.hanbae.globalcart.view.fragment.home;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.widget.Toast;

import com.bit.bitcare.baseapp.presenter.BasePresenter;
import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.hanbae.globalcart.BaseApplication;
import com.hanbae.globalcart.R;
import com.hanbae.globalcart.data.remote.APIMultipartService;
import com.hanbae.globalcart.data.remote.APIService;
import com.hanbae.globalcart.data.remote.NetworkRequest;
import com.hanbae.globalcart.model.RequestInsertItem;
import com.hanbae.globalcart.model.RequestInsertSearch;
import com.hanbae.globalcart.model.ResponseAddFile;
import com.hanbae.globalcart.model.ResponseResult;
import com.hanbae.globalcart.util.PreferenceUtil;
import com.hanbae.globalcart.util.PrepareFilePart;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Subscription;
import timber.log.Timber;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;

public class SellPresenter implements BasePresenter<SellMvpView> {

    private static int SELECT_PICTURE = 0;
    private static int TAKE_PICTURE = 1;

    private Activity mActivity;
    private SellMvpView sellMvpView;
    private Subscription mSubscription;
    private APIMultipartService mAPIMultipartService;
    private ResponseAddFile responseAddFile;
    private String returnString;

    private Uri mUploadFile;
    private String mModifySerialID;
//    private boolean isModify;
    private boolean isFileUpload;
    private boolean isCallToAddFile;

    @Inject
    public SellPresenter(Activity mActivity) {
        ((BaseApplication) mActivity.getApplicationContext()).getApplicationComponent().inject(this);
        this.mActivity = mActivity;
    }

    @Inject
    Gson gson;

    @Inject
    PreferenceUtil preferenceUtil;

    @Inject
    APIService apiService;

    @Override
    public void attachView(SellMvpView view) {
        sellMvpView = view;
        mAPIMultipartService = APIMultipartService.Factory.create(mActivity);

    }

    public PreferenceUtil getPreferenceUtil(){
        return preferenceUtil;
    }

    public Gson getGson (){
        return gson;
    }

    @Override
    public void detachView() {
        sellMvpView = null;
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    private Uri photoUri;
    private String currentPhotoPath;//실제 사진 파일 경로
    private String mImageCaptureName; //이미지 이름

    public String getCurrentPhotoPath(){
        return currentPhotoPath;
    }

    public Uri getPhotoUri(){
        return photoUri;
    }

    private File createImageFile() throws IOException {
        File dir = new File(Environment.getExternalStorageDirectory() + "/globalcart/");
        Timber.e("Environment.getExternalStorageDirectory() : "+Environment.getExternalStorageDirectory());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        mImageCaptureName = timeStamp + ".png";
        File storageDir = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/globalcart/" + mImageCaptureName);
        Timber.e("Environment.getExternalStorageDirectory().getAbsoluteFile() : "+Environment.getExternalStorageDirectory().getAbsoluteFile());
        Timber.e("storageDir : "+storageDir);
        currentPhotoPath = storageDir.getAbsolutePath();
        return storageDir;
    }

    public void takePicture() {//카메라 찍기
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (intent.resolveActivity(mActivity.getPackageManager()) != null) {
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    Timber.e("Error in createImageFile");
                }
                if (photoFile != null) {
                    Timber.e("photoFile : "+photoFile);
                    Timber.e("photoFile.getAbsolutePath() : "+photoFile.getAbsolutePath());
                    Timber.e("mActivity.getPackageName() : "+mActivity.getPackageName());

                    photoUri = FileProvider.getUriForFile(mActivity, "com.hanbae.globalcart.fileprovider", photoFile);
                    Timber.e("photoUri : "+photoUri.toString());
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                    //intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); 다른 앱 간
                    mActivity.startActivityForResult(intent, TAKE_PICTURE);
                }
            }
        }
    }

    public void selectPicture() {//갤러리에서 고르기
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        mActivity.startActivityForResult(intent, SELECT_PICTURE);
    }

    public void addFile(Uri file,int type) {

        Timber.e("upload file uri :"+file);
        if(isFileUpload) {
            mUploadFile = file;
            isCallToAddFile = true;
            deleteFile();
            return;
        }

        sellMvpView.showDialog(mActivity);
        MultipartBody.Part body;

        body = PrepareFilePart.prepareFilePart(mActivity, "image", file, type);

        if (mSubscription != null) mSubscription.unsubscribe();

        mSubscription = NetworkRequest.performAsyncRequest(mAPIMultipartService.addFile(body), (data) -> {
            try {
                String returnString = new String(((Response<ResponseBody>) data).body().bytes());
                String decString = URLDecoder.decode(returnString, "UTF-8");
//                responseAddFile = new ResponseAddFile();
                responseAddFile = gson.fromJson(decString, ResponseAddFile.class);

                Timber.d("응답받은 값 : " + decString);
                Timber.d("응답받은 값(obj) : " + responseAddFile.toString());

                if(responseAddFile.getResult() != null) {
//                    if(isModify) {
//                        mModifySerialID = mAddFileHaribo.getResult().getFileSerialID();
//                    }
                    sellMvpView.addFile(APIService.IMAGE_URL+responseAddFile.getResult().getFileUploadResultVO().getFileName(), responseAddFile.getResult().getFileUploadResultVO().getFileSerialID(), responseAddFile.getResult().getFileUploadResultVO().getFileName());
                    isFileUpload = true;
                } else {
                    Toast.makeText(mActivity, mActivity.getString(R.string.upload_failed), Toast.LENGTH_SHORT).show();
                    isFileUpload = false;
                }

            } catch (Exception e) {
                Timber.e("Exception : " + e.getMessage());
                Toast.makeText(mActivity, "사진 업로드 도중 에러가 발생했습니다.", Toast.LENGTH_SHORT).show();
                FirebaseCrash.report(e);
//                sellMvpView.cancelDialog();
            } finally {
                isFileUpload = false;
                sellMvpView.cancelDialog();
            }
        });

    }

    public void deleteFile() {
        if(!isFileUpload) {
            return;
        }

        sellMvpView.showDialog(mActivity);

        Map map = new HashMap<String,String>();
        Map map2 = new HashMap<String,String>();

//        if(isModify) {
//            map.put(mActivity.getString(R.string.api_params_deleteFile_fileSerialID), mModifySerialID);
//        } else {
            map.put(mActivity.getString(R.string.api_params_deleteFile_fileSerialID), responseAddFile.getResult().getFileUploadResultVO().getFileSerialID());
//        }


//        if (mSubscription != null) mSubscription.unsubscribe();
//        map2.put("param",gson.toJson(map));
//        mSubscription = NetworkRequest.performAsyncRequest(apiService.deleteFile(map2), (data)->{
//            try {
//                String returnString = new String(((Response<ResponseBody>) data).body().bytes());
//                ResponseHaribo responseHaribo = gson.fromJson(returnString, ResponseHaribo.class);
//
//                Timber.d("응답받은 값 : " + returnString);
//                Timber.d("응답받은 값(obj) : " + responseHaribo.toString());
//
                isFileUpload = false;
                if(isCallToAddFile) {
                    isCallToAddFile = false;
//                    addFile(mUploadFile);
                } else {
                    sellMvpView.deleteFile();
                }
//
////                mReportMvpView.onDaysListDraw(mReportListHaribo);
//
//                sellMvpView.cancelDialog();
//            } catch (Exception e) {
////                mHomeMvpView.hideDialog();
//                Timber.e("Exception : " + e.getMessage());
//                FirebaseCrash.report(e);
//            }
//        });
    }
    public void showDeleteFileDialog(boolean modify, String serial) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);

        alertDialogBuilder
                .setMessage("삭제 하시겠습니까?")
                .setCancelable(false)
                .setPositiveButton("네",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                deleteFile();
                            }})
                .setNegativeButton("아니오",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }});

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }



    public void insertItem(String name, String price, String newYN, String color, String comment, List<RequestInsertItem.ItemImageList> itemList){
        RequestInsertItem requestInsertItem = new RequestInsertItem();
        requestInsertItem.setItemName(name);
        if(!"".equals(price)){
            requestInsertItem.setItemPrice(Integer.parseInt(price));
        }
        requestInsertItem.setItemNewYN(newYN);
        requestInsertItem.setItemColor(color);
        requestInsertItem.setItemComment(comment);
        requestInsertItem.setItemImageList(itemList);
        requestInsertItem.setSellUserSerialID(preferenceUtil.getStringExtra("userSID"));

        Timber.e("requestInsertItem : "+ requestInsertItem);
            mSubscription = NetworkRequest.performAsyncRequest(apiService.insertItem(requestInsertItem), (data) -> {
            try {
                String returnString = new String(((Response<ResponseBody>) data).body().bytes());
                String decString = URLDecoder.decode(returnString, "UTF-8");
//                responseAddFile = new ResponseAddFile();
                ResponseResult responseResult = gson.fromJson(decString, ResponseResult.class);

                Timber.d("응답받은 값 : " + decString);
                Timber.d("응답받은 값(obj) : " + responseAddFile.toString());

                if(responseAddFile.getResult() != null) {
                    sellMvpView.addFile(APIService.IMAGE_URL+responseAddFile.getResult().getFileUploadResultVO().getFileName(), responseAddFile.getResult().getFileUploadResultVO().getFileSerialID(), responseAddFile.getResult().getFileUploadResultVO().getFileName());
                    isFileUpload = true;
                } else {
                    Toast.makeText(mActivity, mActivity.getString(R.string.upload_failed), Toast.LENGTH_SHORT).show();
                    isFileUpload = false;
                }
//                mReportMvpView.onDaysListDraw(mReportListHaribo);

                sellMvpView.cancelDialog();
                sellMvpView.resetView();
            } catch (Exception e) {
//                mHomeMvpView.hideDialog();
                Timber.e("Exception : " + e.getMessage());
                FirebaseCrash.report(e);
                sellMvpView.cancelDialog();
            }
        });
    }

//    public void addFile(Uri file) {
////        MultipartBody.Part body = PrepareFilePart.prepareFilePart(mActivity, "image", file);
//
//        if(isFileUpload) {
//            mUploadFile = file;
//            isCallToAddFile = true;
//            deleteFile();
//            return;
//        }
//
//        sellMvpView.showDialog(mActivity);
//
//        MultipartBody.Part body = PrepareFilePart.prepareFilePart(mActivity, "image", file);
//
//        if (mSubscription != null) mSubscription.unsubscribe();
//
//        mSubscription = NetworkRequest.performAsyncRequest(mAPIMultipartService.addFile(body), (data) -> {
//            try {
//                String returnString = new String(((Response<ResponseBody>) data).body().bytes());
//                String decString = URLDecoder.decode(returnString, "UTF-8");
//
////                mAddFileHaribo = gson.fromJson(decString, AddFileHaribo.class);
//
//                Timber.d("응답받은 값 : " + decString);
////                Timber.d("응답받은 값(obj) : " + mAddFileHaribo.toString());
//
//                if(mAddFileHaribo.getResult() != null) {
//                    if(isModify) {
//                        mModifySerialID = mAddFileHaribo.getResult().getFileSerialID();
//                    }
//                    sellMvpView.addFile(APIService.IMAGE_URL + mAddFileHaribo.getResult().getUrl(), mAddFileHaribo.getResult().getFileSerialID());
//                    isFileUpload = true;
//                } else {
//                    Toast.makeText(mActivity, mActivity.getString(R.string.diaryaddpresenter_picture_upload_failed), Toast.LENGTH_SHORT).show();
//                    isFileUpload = false;
//                }
//
//
////                mReportMvpView.onDaysListDraw(mReportListHaribo);
//
//                mDiaryAddMvpView.cancelDialog();
//            } catch (Exception e) {
////                mHomeMvpView.hideDialog();
//                Timber.e("Exception : " + e.getMessage());
//                FirebaseCrash.report(e);
//            }
//        });
//
//    }
//
//    public void deleteFile() {
//        if(!isFileUpload) {
//            return;
//        }
//
//        mDiaryAddMvpView.showDialog(mActivity);
//
//        Map map = new HashMap<String,String>();
//        Map map2 = new HashMap<String,String>();
//
//        if(isModify) {
//            map.put(mActivity.getString(R.string.api_params_deleteFile_fileSerialID), mModifySerialID);
//        } else {
//            map.put(mActivity.getString(R.string.api_params_deleteFile_fileSerialID), mAddFileHaribo.getResult().getFileSerialID());
//        }
//
//
//        if (mSubscription != null) mSubscription.unsubscribe();
//        map2.put("param",gson.toJson(map));
//        mSubscription = NetworkRequest.performAsyncRequest(mAPIService.deleteFile(map2), (data)->{
//            try {
//                String returnString = new String(((Response<ResponseBody>) data).body().bytes());
//                ResponseHaribo responseHaribo = gson.fromJson(returnString, ResponseHaribo.class);
//
//                Timber.d("응답받은 값 : " + returnString);
//                Timber.d("응답받은 값(obj) : " + responseHaribo.toString());
//
//                isFileUpload = false;
//                if(isCallToAddFile) {
//                    isCallToAddFile = false;
//                    addFile(mUploadFile);
//                } else {
//                    mDiaryAddMvpView.deleteFile();
//                }
//
////                mReportMvpView.onDaysListDraw(mReportListHaribo);
//
//                mDiaryAddMvpView.cancelDialog();
//            } catch (Exception e) {
////                mHomeMvpView.hideDialog();
//                Timber.e("Exception : " + e.getMessage());
//                FirebaseCrash.report(e);
//            }
//        });
//    }

}