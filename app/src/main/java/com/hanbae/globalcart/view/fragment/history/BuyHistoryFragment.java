package com.hanbae.globalcart.view.fragment.history;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.hanbae.globalcart.R;
import com.hanbae.globalcart.view.base.AppBaseFragment;
import com.hanbae.globalcart.view.fragment.home.BuyMvpView;
import com.hanbae.globalcart.view.fragment.home.BuyPresenter;

import butterknife.BindView;
import butterknife.OnClick;

public class BuyHistoryFragment extends AppBaseFragment implements BuyHistoryMvpView {

    private BuyHistoryPresenter buyHistoryPresenter;

    @BindView(R.id.et_home_buy_name) TextInputEditText et_home_buy_name;
    @BindView(R.id.et_home_buy_price) TextInputEditText et_home_buy_price;
    @BindView(R.id.rg_home_buy) RadioGroup rg_home_buy;
    @BindView(R.id.rb_home_buy_new) RadioButton rb_home_buy_new;
    @BindView(R.id.rb_home_buy_used) RadioButton rb_home_buy_used;

    @OnClick({R.id.btn_home_buy_request_regist})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.btn_home_buy_request_regist:
//
//                if("".equals(et_home_buy_name.getText().toString().trim())){
//
//                }

                String productTypeYN;
                int checkedId = rg_home_buy.getCheckedRadioButtonId();
                if(checkedId==rb_home_buy_new.getId()){
                    productTypeYN = "Y";
                }else {
                    productTypeYN = "N";
                }

                buyHistoryPresenter.insertSearch(et_home_buy_name.getText().toString().trim(),et_home_buy_price.getText().toString().trim(),productTypeYN);
                break;
        }
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.fragment_home_buy;
    }

    @Override
    protected void onViewReady(@Nullable Bundle savedInstanceState) {
        buyHistoryPresenter = new BuyHistoryPresenter(getActivity());
        buyHistoryPresenter.attachView(this);
    }

    @Override
    public void resetView(){
        et_home_buy_name.setText("");
        et_home_buy_price.setText("");
        rb_home_buy_new.setChecked(true);
        rb_home_buy_used.setChecked(false);
        Toast.makeText(getContext(), "구매하시려는 물품이 등록되었습니다.", Toast.LENGTH_SHORT).show();

        et_home_buy_name.requestFocus();

    }
}

