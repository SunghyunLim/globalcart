package com.hanbae.globalcart.view.fragment.home;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.hanbae.globalcart.R;
import com.hanbae.globalcart.model.RequestInsertItem;
import com.hanbae.globalcart.view.activity.login.LoginActivity;
import com.hanbae.globalcart.view.base.AppBaseFragment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

public class SellFragment extends AppBaseFragment implements SellMvpView {

    private SellPresenter sellPresenter;
    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 10;
    private String fileSerialID;
    private String fileName;
    private List<RequestInsertItem.ItemImageList> itemImageList;

//    @Getter
//    @Setter
//    private String pictureFilePath;

    @BindView(R.id.et_home_sell_name) TextInputEditText et_home_sell_name;
    @BindView(R.id.et_home_sell_price) TextInputEditText et_home_sell_price;
    @BindView(R.id.rg_home_sell) RadioGroup rg_home_sell;
    @BindView(R.id.rb_home_sell_new) RadioButton rb_home_buy_new;
    @BindView(R.id.rb_home_sell_used) RadioButton rb_home_buy_used;
    @BindView(R.id.et_home_sell_color) TextInputEditText et_home_sell_color;
    @BindView(R.id.et_home_sell_comment) TextInputEditText et_home_sell_comment;
    @BindView(R.id.iv_home_sell_add_picture) ImageView iv_home_sell_add_picture;
    @BindView(R.id.tv_home_sell_add_picture) TextView tv_home_sell_add_picture;
    @BindView(R.id.rl_home_sell_pic_cancel) RelativeLayout rl_home_sell_pic_cancel;
    @BindView(R.id.ll_home_sell_add_picture) LinearLayout ll_home_sell_add_picture; //
    @BindView(R.id.iv_home_sell_picture) ImageView iv_home_sell_picture;

    @OnClick({R.id.btn_home_sell_request_regist, R.id.iv_home_sell_add_picture, R.id.tv_home_sell_add_picture, R.id.iv_home_sell_picture_cancel})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.btn_home_sell_request_regist:
                if("".equals(et_home_sell_name.getText().toString().trim())){
                    Toast.makeText(getActivity(),"물품명을 입력해 주세요.",Toast.LENGTH_SHORT).show();
                    break;
                }

                if("".equals(et_home_sell_price.getText().toString())) {
                    Toast.makeText(getActivity(),"물품가격을 입력해 주세요.",Toast.LENGTH_SHORT).show();
                    break;
                }

                insertFile();
                break;

            case R.id.iv_home_sell_add_picture:
            case R.id.tv_home_sell_add_picture:
                if(checkPermissions()){
                    performDialog();
                }
                break;

            case R.id.iv_home_sell_picture_cancel:
                deleteFile();
                break;
        }
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.fragment_home_sell;
    }

    @Override
    protected void onViewReady(@Nullable Bundle savedInstanceState) {
        sellPresenter = new SellPresenter(getActivity());
        sellPresenter.attachView(this);
        itemImageList = new ArrayList<RequestInsertItem.ItemImageList>();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Timber.e("requestCode : "+requestCode);
        Timber.e("resultCode : "+resultCode);
        if(resultCode != RESULT_OK) {
            return;
        }

        switch(requestCode) {
            case 0 :
                Uri image = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(image, filePath, null, null, null);
                cursor.moveToFirst();
                String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
                Timber.i("imagePath: " + imagePath);
                if(imagePath != null && (imagePath.contains(".png") || imagePath.contains(".jpg"))) {
                    sellPresenter.addFile(image, 0);
                } else {
                    Toast.makeText(getActivity(), getString(R.string.upload_failed), Toast.LENGTH_SHORT).show();
                }
                break;

            case 1 :
                Uri takeImage = sellPresenter.getPhotoUri();

                String takeImagePath = sellPresenter.getCurrentPhotoPath();
                Timber.i("takeImagePath: " + takeImagePath);
                if(takeImagePath != null && (takeImagePath.contains(".png") || takeImagePath.contains(".jpg"))) {
                    sellPresenter.addFile(takeImage, 1);
                } else {
                    Toast.makeText(getActivity(), getString(R.string.upload_failed), Toast.LENGTH_SHORT).show();
                }
//                Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath);
//                ExifInterface exif = null;
//                try {
//                    exif = new ExifInterface(currentPhotoPath);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                int exifOrientation;
//                int exifDegree;
//
//                if (exif != null) {
//                    exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
//                    exifDegree = exifOrientationToDegrees(exifOrientation);
//                } else {
//                    exifDegree = 0;


//                    String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
//                    Timber.i("imagePath: " + imagePath);
//                    if(imagePath != null && (imagePath.contains(".png") || imagePath.contains(".jpg"))) {
//                        sellPresenter.addFile(image);
//                    } else {
//                        Toast.makeText(getActivity(), getString(R.string.upload_failed), Toast.LENGTH_SHORT).show();
//                    }
//                }
                break;

                default :


        }
    }



    private int exifOrientationToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    private String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}; //권한 설정 변수
    public static final int MULTIPLE_PERMISSIONS = 101; //권한 동의 여부 문의 후 CallBack 함수에 쓰일 변수

    private boolean checkPermissions() {
        int result;
        List<String> permissionList = new ArrayList<>();
        for (String pm : permissions) {
            result = ContextCompat.checkSelfPermission(getActivity(), pm);
            if (result != PackageManager.PERMISSION_GRANTED) { //사용자가 해당 권한을 가지고 있지 않을 경우 리스트에 해당 권한명 추가
                permissionList.add(pm);
            }
        }
        if (!permissionList.isEmpty()) { //권한이 추가되었으면 해당 리스트가 empty가 아니므로 request 즉 권한을 요청합니다.
            ActivityCompat.requestPermissions(getActivity(), permissionList.toArray(new String[permissionList.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void performDialog(){
        new MaterialDialog.Builder(getActivity())
                .title("선택해주세요")
                .positiveText("카메라")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            // 권한이 없을경우

                            // 최초 권한 요청인지, 혹은 사용자에 의한 재요청인지 확인
                            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                                // 사용자가 임의로 권한을 취소시킨 경우
                                // 권한 재요청
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                            } else {
                                // 최초로 권한을 요청하는 경우(첫실행)
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                            }
                        } else {
                            sellPresenter.takePicture();
                        }
                    }
                })
                .negativeText("갤러리")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        sellPresenter.selectPicture();
                    }
                })
                .show();
    }
//    public void permissionCheckReadContents() {
//        // 갤러리 사용 권한 체크( 사용권한이 없을경우 -1 )
//        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            // 권한이 없을경우
//
//            // 최초 권한 요청인지, 혹은 사용자에 의한 재요청인지 확인
//            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
//                // 사용자가 임의로 권한을 취소시킨 경우
//                // 권한 재요청
//                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
//
//            } else {
//                // 최초로 권한을 요청하는 경우(첫실행)
//                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
//            }
//        } else {
//            // 사용 권한이 있음을 확인한 경우
//            new MaterialDialog.Builder(getActivity())
//                    .title("선택해주세요")
//                    .positiveText("카메라")
//                    .onPositive(new MaterialDialog.SingleButtonCallback() {
//                        @Override
//                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                                // 권한이 없을경우
//
//                                // 최초 권한 요청인지, 혹은 사용자에 의한 재요청인지 확인
//                                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
//                                    // 사용자가 임의로 권한을 취소시킨 경우
//                                    // 권한 재요청
//                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
//
//                                } else {
//                                    // 최초로 권한을 요청하는 경우(첫실행)
//                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
//                                }
//                            } else {
//                                sellPresenter.takePicture();
//                            }
//                        }
//                    })
//                    .negativeText("갤러리")
//                    .onNegative(new MaterialDialog.SingleButtonCallback() {
//                        @Override
//                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                            sellPresenter.selectPicture();
//                        }
//                    })
//                    .show();
//        }
//    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                // 갤러리 사용권한에 대한 콜백을 받음
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // 권한 동의 버튼 선택
                  performDialog();
                } else {
                    // 사용자가 권한 동의를 안함
                    // 권한 동의안함 버튼 선택
                    Toast.makeText(getActivity(), getString(R.string.permission_confirm), Toast.LENGTH_LONG).show();
                    getActivity().finish();
                }
                return;
            }
        }
    }

    @Override
    public void addFile(String url, String serial, String fileName) {
        RequestInsertItem.ItemImageList item = new RequestInsertItem.ItemImageList();
        item.setFileSerialID(serial);
        item.setFileName(fileName);
        itemImageList.add(item);

        fileSerialID = serial;
        rl_home_sell_pic_cancel.setVisibility(View.VISIBLE);
        ll_home_sell_add_picture.setVisibility(View.GONE);
        Timber.i("url: " + url);
        Glide.with(mContext).load(url).into(iv_home_sell_picture);
    }

    @Override
    public void deleteFile() {
        itemImageList = new ArrayList<RequestInsertItem.ItemImageList>();

        rl_home_sell_pic_cancel.setVisibility(View.GONE);
        ll_home_sell_add_picture.setVisibility(View.VISIBLE);

//        iv_home_sell_picture.setImageResource(android.R.color.transparent);
        iv_home_sell_picture.setImageDrawable(null);

    }

    private void insertFile(){
        String productTypeYN;
        int checkedId = rg_home_sell.getCheckedRadioButtonId();
        if(checkedId==rb_home_buy_new.getId()){
            productTypeYN = "Y";
        }else {
            productTypeYN = "N";
        }

        sellPresenter.insertItem(et_home_sell_name.getText().toString().trim(),et_home_sell_price.getText().toString().trim(),productTypeYN,et_home_sell_color.getText().toString().trim(), et_home_sell_comment.getText().toString().trim(), itemImageList);
    }

    public void resetView(){
        et_home_sell_name.setText("");
        et_home_sell_price.setText("");
        et_home_sell_color.setText("");
        et_home_sell_comment.setText("");

        itemImageList = new ArrayList<RequestInsertItem.ItemImageList>();
        rb_home_buy_new.setChecked(true);
        rb_home_buy_used.setChecked(false);

        deleteFile();
        Toast.makeText(getContext(), "판매하시려는 물품이 등록되었습니다.", Toast.LENGTH_SHORT).show();

        et_home_sell_name.requestFocus();
    }

}
