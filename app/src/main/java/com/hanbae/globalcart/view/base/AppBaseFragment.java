package com.hanbae.globalcart.view.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.bit.bitcare.baseapp.ui.fragment.BaseFragment;
import com.hanbae.globalcart.BaseApplication;
import com.hanbae.globalcart.di.component.ApplicationComponent;
import com.hanbae.globalcart.di.component.DaggerApplicationComponent;
import com.hanbae.globalcart.di.module.ApplicationModule;

/**
 * Created by acid on 2016-12-15.
 */

public abstract class AppBaseFragment extends BaseFragment implements AppBaseMvpView {

    private ApplicationComponent mApplicationComponent;
    private ProgressDialog mCustomProgressDialog;
    private Activity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplicationComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(getApp())).build();
        mApplicationComponent.inject(this);
    }

    protected BaseApplication getApp() {
        return (BaseApplication) getActivity().getApplicationContext();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();
        cancelDialog();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dismissDialog();
    }

    @Override
    public void showDialog(Activity activity) {
        this.activity = activity;
        if (mCustomProgressDialog == null) {
            mCustomProgressDialog = new ProgressDialog(activity);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.show();
        }
    }

    @Override
    public void hideDialog() {
        if (mCustomProgressDialog != null) {
            mCustomProgressDialog.hide();

        }
    }

    @Override
    public void cancelDialog() {
        if (mCustomProgressDialog != null) {
            mCustomProgressDialog.cancel();

        }
    }

    @Override
    public void dismissDialog() {
        if (mCustomProgressDialog != null) {
            mCustomProgressDialog.dismiss();

        }
    }
}

