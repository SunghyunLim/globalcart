package com.hanbae.globalcart.view.fragment.home;

import com.hanbae.globalcart.view.base.AppBaseMvpView;

/**
 * Created by acid on 2018-04-24.
 */

public interface BuyMvpView extends AppBaseMvpView {
    public void resetView();
}
