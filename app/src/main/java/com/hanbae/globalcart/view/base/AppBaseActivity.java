package com.hanbae.globalcart.view.base;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.widget.Toast;
import com.bit.bitcare.baseapp.ui.BaseActivity;
import com.bit.bitcare.baseapp.ui.fragment.BaseFragment;
import com.hanbae.globalcart.BaseApplication;
import com.hanbae.globalcart.di.component.ActivityComponent;
import com.hanbae.globalcart.di.component.DaggerActivityComponent;
import com.hanbae.globalcart.di.module.ActivityModule;
import com.hanbae.globalcart.util.PreferenceUtil;

import java.util.List;
import javax.inject.Inject;

import butterknife.ButterKnife;
import timber.log.Timber;

public abstract class AppBaseActivity extends BaseActivity implements AppBaseMvpView {

    private ActivityComponent mComponent;
    public ProgressDialog mCustomProgressDialog;

    @Inject
    PreferenceUtil preferenceUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mComponent = DaggerActivityComponent.builder().applicationComponent(getApp().getApplicationComponent()).activityModule(new ActivityModule(this)).build();
        getComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(getResourceLayout());
        ButterKnife.bind(this);
        Timber.tag(getClass().getSimpleName());
        mInflater = LayoutInflater.from(mContext);
        onViewReady(savedInstanceState);
        Timber.e("AppBaseActivity");
    }

    protected ActivityComponent getComponent() {
        return mComponent;
    }

    protected BaseApplication getApp() {
        return (BaseApplication) getApplicationContext();
    }

    protected Fragment mContent;

    public PreferenceUtil getPreferenceUtil() {
        return preferenceUtil;
    }

    @Inject
    protected List<Class<? extends AppBaseFragment>> mBeforeFragment;

    //프레그먼트 이동시 이전 프레그먼트 저장 안함
    protected void replaceFragment(Class<? extends AppBaseFragment> destinationFragment) {
        BaseFragment fragment = null;
        try {
            fragment = destinationFragment.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fragment == null) {
            throw new IllegalStateException("cannot start fragment. " + destinationFragment.getName());
        }
        mContent = fragment;
//        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    //프레그먼트 이동시 이전 프레그먼트 저장 안함
    protected void replaceFragment(int contentViewId, Class<? extends AppBaseFragment> destinationFragment) {
        BaseFragment fragment = null;
        try {
            fragment = destinationFragment.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fragment == null) {
            throw new IllegalStateException("cannot start fragment. " + destinationFragment.getName());
        }
        mContent = fragment;
        getSupportFragmentManager().beginTransaction().replace(contentViewId, fragment).commit();
    }

    //프레그먼트 이동시 이전 프레그먼트 저장 안함
    protected void replaceFragment(Class<? extends AppBaseFragment> destinationFragment, Bundle extras) {
        BaseFragment fragment = null;
        try {
            fragment = destinationFragment.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fragment == null) {
            throw new IllegalStateException("cannot start fragment. " + destinationFragment.getName());
        }

        fragment.setArguments(extras);
        mContent = fragment;
//        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    //프레그먼트 이동시 이전 프레그먼트 저장 안함
    protected void replaceFragment(int contentViewId, Class<? extends AppBaseFragment> destinationFragment, Bundle extras) {
        BaseFragment fragment = null;
        try {
            fragment = destinationFragment.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fragment == null) {
            throw new IllegalStateException("cannot start fragment. " + destinationFragment.getName());
        }

        fragment.setArguments(extras);
        mContent = fragment;
        getSupportFragmentManager().beginTransaction().replace(contentViewId, fragment).commit();
    }

    //프레그먼트 이동시 이전 프레그먼트 저장 함
    protected void replaceFragment(Class<? extends AppBaseFragment> sourceFragment, Class<? extends AppBaseFragment> destinationFragment) {
        BaseFragment fragment = null;
        try {
            fragment = destinationFragment.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fragment == null) {
            throw new IllegalStateException("cannot start fragment. " + destinationFragment.getName());
        }
        mContent = fragment;
//        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
        putCurrentFragment(sourceFragment);
    }

    //프레그먼트 이동시 이전 프레그먼트 저장 함
    protected void replaceFragment(Class<? extends AppBaseFragment> sourceFragment, Class<? extends AppBaseFragment> destinationFragment, Bundle extras) {
        BaseFragment fragment = null;
        try {
            fragment = destinationFragment.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fragment == null) {
            throw new IllegalStateException("cannot start fragment. " + destinationFragment.getName());
        }

        fragment.setArguments(extras);
        mContent = fragment;
//        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
        putCurrentFragment(sourceFragment);
    }

    protected void putCurrentFragment(Class<? extends AppBaseFragment> fragment) {
        mBeforeFragment.add(fragment);
    }

    protected Class<? extends AppBaseFragment> getLastFragment() {
        Class<? extends AppBaseFragment> fragment = mBeforeFragment.get(mBeforeFragment.size() - 1);
        return fragment;
    }

    public void enterNewTaskActivity(Class<? extends AppBaseActivity> cls) {
        Intent intent = new Intent(this, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(0, 0);
        Timber.e("enterNewTaskActivity cls");
    }

    public void enterActivity(Class<? extends AppBaseActivity> cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
        overridePendingTransition(0, 0);
        Timber.e("enterActivity cls");
    }

    public void enterActivity(Class<? extends AppBaseActivity> cls, Bundle extras) {
        Intent intent = new Intent(this, cls);
        intent.putExtras(extras);
        startActivity(intent);
        overridePendingTransition(0, 0);
        Timber.e("enterActivity cls & bundle");
    }

    public void enterActivityForResult(Class<? extends AppBaseActivity> cls, int requestCode) {
        Intent intent = new Intent(this, cls);
        startActivityForResult(intent, requestCode);
        overridePendingTransition(0, 0);
        Timber.e("enterActivity cls & requestCode");
    }

    public void enterActivityForResult(Class<? extends AppBaseActivity> cls, int requestCode, Bundle extras) {
        Intent intent = new Intent(this, cls);
        intent.putExtras(extras);
        startActivityForResult(intent, requestCode);
        overridePendingTransition(0, 0);
        Timber.e("enterActivity cls & requestCode & bundle");
    }

    @Override
    public void showDialog(Activity activity) {
        if (mCustomProgressDialog == null) {
            mCustomProgressDialog = new ProgressDialog(activity);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.show();
        }
    }

    @Override
    public void hideDialog() {
        if (mCustomProgressDialog != null) {
            mCustomProgressDialog.hide();
        }
    }

    @Override
    public void cancelDialog() {
        if (mCustomProgressDialog != null) {
            mCustomProgressDialog.cancel();
        }
    }

    @Override
    public void dismissDialog() {
        if (mCustomProgressDialog != null) {
            mCustomProgressDialog.dismiss();
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        cancelDialog();
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissDialog();
    }

    /* 모든 액티비티 종료시키기 위한 구간 */

    public static final String FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION = "com.hrupin.FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION";
    private BaseActivityReceiver baseActivityReceiver = new BaseActivityReceiver();
    public static final IntentFilter INTENT_FILTER = createIntentFilter();

    private static IntentFilter createIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION);
        return filter;
    }

    protected void registerBaseActivityReceiver() {
        registerReceiver(baseActivityReceiver, INTENT_FILTER);
    }

    protected void unRegisterBaseActivityReceiver() {
        unregisterReceiver(baseActivityReceiver);
    }

    public class BaseActivityReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION)) {
                finish();
            }
        }
    }

    protected void closeAllActivities() {
        sendBroadcast(new Intent(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION));
    }

}