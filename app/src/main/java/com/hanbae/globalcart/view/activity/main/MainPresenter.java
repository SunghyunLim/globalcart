package com.hanbae.globalcart.view.activity.main;


import android.content.Context;
import android.provider.Settings;

import com.bit.bitcare.baseapp.presenter.BasePresenter;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.hanbae.globalcart.BaseApplication;
import com.hanbae.globalcart.data.remote.APIService;
import com.hanbae.globalcart.util.PreferenceUtil;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import rx.Subscription;

public class MainPresenter implements BasePresenter<MainMvpView> {

    private Context context;
    private MainMvpView mainMvpView;
    private Subscription mSubscription;
    private String returnString;
    private String userIDType;

    @Inject
    public MainPresenter(Context context) {
        ((BaseApplication) context.getApplicationContext()).getApplicationComponent().inject(this);
        this.context = context;
    }

    @Inject
    Gson gson;

    @Inject
    PreferenceUtil preferenceUtil;

    @Inject
    APIService apiService;

    @Override
    public void attachView(MainMvpView view) {
        mainMvpView = view;
    }

    public Gson getGson (){
        return gson;
    }

    @Override
    public void detachView() {
        mainMvpView = null;
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    // 1st - 기존에 카카오톡 혹은 페이스북 ID로 등록이 되었는지 확인 -> 확인 후 로그인,
    public void checkUserAccountInfo(String uid) {

        userIDType = preferenceUtil.getStringExtra("authVendor");
        if(userIDType.equals("")){
            userIDType = "K";
        }
        Map paramMap = new HashMap<String, String>();
        paramMap.put("serviceSerialID", "201701");
        paramMap.put("accountID", uid);
        paramMap.put("email", preferenceUtil.getStringExtra("email"));
        paramMap.put("userIDType", userIDType);
        paramMap.put("deviceType", "AND");
        paramMap.put("deviceSID", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
        paramMap.put("deviceRegID", FirebaseInstanceId.getInstance().getToken());

        Map requestMap = new HashMap<String, String>();
        requestMap.put("param", gson.toJson(paramMap));

        //사용자 정보 등록 했는지 확인 후 사용자 정보 추가 혹은 사용자 정보 가져오기 실행
//        if (mSubscription != null) mSubscription.unsubscribe();
//        mSubscription = NetworkRequest.performAsyncRequest(apiService.getAccountInfo(requestMap), (data) -> {
//            try {
//                returnString = new String(((Response<ResponseBody>) data).body().bytes());
//                AccountHaribo haribo = gson.fromJson(returnString, AccountHaribo.class);
//                Timber.d("응답받은 값 : " + returnString);
//                Timber.d("응답받은 값(obj) : " + haribo.toString());
//                Timber.d("gson.toJson(haribo.getResult()) : " + gson.toJson(haribo.getResult()));
//                preferenceUtil.putStringExtra("userAccountData", gson.toJson(haribo.getResult()));
//                getLogin(uid, userIDType);
//            } catch (Exception e) {
//                Timber.e("Exception : " + e.getMessage());
//                FirebaseCrash.report(e);
//            }
//        }, (error) -> {
//        });
    }

}