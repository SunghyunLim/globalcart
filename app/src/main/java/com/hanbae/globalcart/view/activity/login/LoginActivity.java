package com.hanbae.globalcart.view.activity.login;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.hanbae.globalcart.R;
import com.hanbae.globalcart.view.activity.main.MainActivity;
import com.hanbae.globalcart.view.base.AppBaseActivity;
import com.hanbae.globalcart.view.base.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.OnClick;
import timber.log.Timber;

import static android.widget.Toast.LENGTH_SHORT;

public class LoginActivity extends AppBaseActivity implements LoginMvpView {

    private LoginPresenter loginPresenter;

    private Button login_google_button;
    private Button login_facebook_button;
    private Button login_email_button;

    private FirebaseAuth mAuth;
    private CallbackManager callbackManager; //use facebook login

    private static final int RC_SIGN_IN = 9001; //use google login
    private GoogleSignInClient mGoogleSignInClient; //use google login


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // [START config_signin]
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // [END config_signin]

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_splash;
    }


    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        loginPresenter = new LoginPresenter(this);
        loginPresenter.attachView(this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Timber.e("requestCode : " + requestCode);
        Timber.e("resultCode : " + resultCode);
        Timber.e("data : " + data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Timber.e("Google sign in failed", e);
                // [START_EXCLUDE]
//                updateUI(null);
                // [END_EXCLUDE]
            }
        } else {
            // Pass the activity result back to the Facebook SDK
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }


    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            Timber.e("user.getDisplayName() : " + currentUser.getDisplayName());
            Timber.e("user.getDisplayName() : " + currentUser.getUid());
            loginPresenter.login(currentUser.getUid());
        } else {
            setContentView(R.layout.activity_login);

            Button login_googleplus_button = findViewById(R.id.login_googleplus_button);
            login_googleplus_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Timber.e("구글 로그인 버튼 눌림!");
                    getPreferenceUtil().putStringExtra("authVendor", "G");
                    googleSignIn();
                }
            });

            Button login_facebook_button = findViewById(R.id.login_facebook_button);
            login_facebook_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Timber.e("페이스북 로그인 버튼 눌림!");
                    getPreferenceUtil().putStringExtra("authVendor", "F");
                    facebookLogin();
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Timber.e("onResume : " + getPreferenceUtil().getStringExtra("authVendor"));

        if ("G".equals(getPreferenceUtil().getStringExtra("authVendor"))) {
            Timber.e("onResume Google");
        } else if ("F".equals(getPreferenceUtil().getStringExtra("authVendor"))) {
            Timber.e("onResume Facebook");
//            facebookLogin();
        } else if ("E".equals(getPreferenceUtil().getStringExtra("authVendor"))) {
            Timber.e("onResume Email");
        } else {
            Timber.e("onResume else");
//            initButton();
        }
    }


//    @OnClick({R.id.login_facebook_button, R.id.login_googleplus_button})
//    public void clickMenu(View v) {
//        switch (v.getId()) {
//            case R.id.login_googleplus_button:
//                Timber.e("구글 로그인 버튼 눌림!");
//                getPreferenceUtil().putStringExtra("authVendor", "G");
//                googleSignIn();
//                break;
//            case R.id.login_facebook_button:
//                Timber.e("페이스북 로그인 버튼 눌림!");
//                getPreferenceUtil().putStringExtra("authVendor", "F");
//                facebookLogin();
//                break;

//            case R.id.logout_button:
//                Timber.e("로그아웃 버튼 눌림!");
//                getPreferenceUtil().putStringExtra("authVendor", "");
//
//                FirebaseAuth.getInstance().signOut();
//                break;


//                if("G".equals(getPreferenceUtil().getStringExtra("authVendor"))) {
//                    Timber.e("onResume Google");
//                }else if("F".equals(getPreferenceUtil().getStringExtra("authVendor"))){
//                    Timber.e("onResume Facebook");
//                    facebookLogin();
//                }else if("E".equals(getPreferenceUtil().getStringExtra("authVendor"))){
//                    Timber.e("onResume Email");
//                }else{
//                    Timber.e("onResume else");
//            initButton();


//                if (isOpened) {
//                    Timber.e("mCurrentPage : "+mCurrentPage);
//                    closeMenu();
//                    if(tempInputBpFragmentPresenter != null){
//                        tempInputBpFragmentPresenter.stopUA651BLE();
//                        tempInputBpFragmentPresenter.stopOmron();
//                        tempInputBpFragmentPresenter = null;
//                    }
//                } else {
//                    tempInputBpFragmentPresenter = null;
//                    openMenu();
//                }
//                break;
//        }
//    }

//    private  void initButton(){
//        facebookLoginInit();
//    }
//
//    //////Facebook login//////
//    private void facebookLoginInit(){
//        login_facebook_button = (Button) findViewById(R.id.login_facebook_button);
//        login_facebook_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
//    }

    public void facebookLogin() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_photos", "email", "public_profile", "user_posts"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        Timber.e("onSuccess");
                        handleFacebookAccessToken(loginResult.getAccessToken());
//                        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
//                            @Override
//                            public void onCompleted(JSONObject object, GraphResponse response) {
//                                if (response.getError() != null) {
//
//                                } else {
//                                    String email = "";
//                                    try {
//                                        email = object.getString("email");
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                    }
//                                    Log.i("TAG", "user: " + object.toString());
//                                    Log.i("TAG", "AccessToken: " + loginResult.getAccessToken().getToken());
//                                    Log.i("TAG", "UserId: " + loginResult.getAccessToken().getUserId());
//                                    getPreferenceUtil().putStringExtra("email", email);
//                                    getPreferenceUtil().putStringExtra("accountID", String.valueOf(loginResult.getAccessToken().getUserId()));
//                                    loginPresenter.checkUserAccountInfo(String.valueOf(loginResult.getAccessToken().getUserId()));
//                                }
//                            }
//                        });

//                        Bundle parameters = new Bundle();
//                        parameters.putString("fields", "id,name,email,gender,birthday");
//                        graphRequest.setParameters(parameters);
//                        graphRequest.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Timber.e("onCancel");
                        getPreferenceUtil().putStringExtra("authVendor", "");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Timber.e("onError");
                        getPreferenceUtil().putStringExtra("authVendor", "");
                    }
                });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Timber.d("handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Timber.e("signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
//                            updateUI(user);
                            Timber.e("user.getDisplayName() : " + user.getDisplayName());
                            Timber.e("user.getDisplayName() : " + user.getUid());

                            loginPresenter.isRegist(user.getUid(), user.getDisplayName());
                        } else {
                            // If sign in fails, display a message to the user.
                            Timber.e("signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    LENGTH_SHORT).show();
                            Timber.e("sign fail");
                        }

                        // ...
                    }
                });
    }


    //////Google login//////

    // [START signin]
    private void googleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signin]

    private void googleSignOut() {
        // Firebase sign out
        mAuth.signOut();

        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
    }

    //해당 앱과 연결 끊음
//    private void revokeAccess() {
//        // Firebase sign out
//        mAuth.signOut();
//
//        // Google revoke access
//        mGoogleSignInClient.revokeAccess().addOnCompleteListener(this,
//                new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//
//                    }
//                });
//    }

    // [START auth_with_google]
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Timber.e("firebaseAuthWithGoogle:" + acct.getId());
        // [START_EXCLUDE silent]
        showDialog(this);
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Timber.e("signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            Timber.e("user.getEmail() : " + user.getEmail());
                            Timber.e("user.getDisplayName() : " + user.getDisplayName());
                            Timber.e("user.getUid() : " + user.getUid());
//                            Timber.e("user.getPhoneNumber() : " + getPhoneNumber());

                            loginPresenter.isRegist(user.getUid(), user.getDisplayName());
//                            enterMainActivity();
//                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Timber.e("signInWithCredential:failure", task.getException());
//                            Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
//                            updateUI(null);
                        }

                        // [START_EXCLUDE]
                        hideDialog();
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END auth_with_google]


    @Override
    public void enterMainActivity() {
        enterActivity(MainActivity.class);
        finish();
    }

//    public String getPhoneNumber() {
//        TelephonyManager telephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        String phoneNumber = "";
//
//        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
//
//        if(permissionCheck== PackageManager.PERMISSION_DENIED){
//
//            // 권한 없음
//        }else{
//            // 권한 있음
//        }
//
//
//        try {
//            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//                // TODO: Consider calling
//                //    ActivityCompat#requestPermissions
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for ActivityCompat#requestPermissions for more details.
//
//                if (telephony.getLine1Number() != null) {
//                    phoneNumber = telephony.getLine1Number();
//                    Timber.e("if phoneNumber : " + phoneNumber);
//                } else {
//                    if (telephony.getSimSerialNumber() != null) {
//                        phoneNumber = telephony.getSimSerialNumber();
//                        Timber.e("else phoneNumber : " + phoneNumber);
//                    }
//                }
//
//                phoneNumber.replace("+82","0");
//                Timber.e("phoneNumber : " + phoneNumber);
//                return phoneNumber;
//            }
//
//        }
//        catch(SecurityException se){
//            if (telephony.getSimSerialNumber() != null) {
//                phoneNumber = telephony.getSimSerialNumber();
//                Timber.e("else phoneNumber : " + phoneNumber);
//            }
//        }
//        catch(Exception e) {
//            e.printStackTrace();
//        }
//
//        return phoneNumber;
//    }
}
