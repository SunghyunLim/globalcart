package com.hanbae.globalcart.view.fragment.home;

import com.hanbae.globalcart.view.base.AppBaseMvpView;

/**
 * Created by acid on 2018-04-24.
 */

public interface SellMvpView extends AppBaseMvpView {
//    public void exit();
    public void addFile(String url, String serial, String fileName);
    public void deleteFile();
    public void resetView();

}
