package com.hanbae.globalcart.view.fragment.history;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.Toast;

import com.bit.bitcare.baseapp.presenter.BasePresenter;
import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.hanbae.globalcart.BaseApplication;
import com.hanbae.globalcart.R;
import com.hanbae.globalcart.data.remote.APIMultipartService;
import com.hanbae.globalcart.data.remote.APIService;
import com.hanbae.globalcart.data.remote.NetworkRequest;
import com.hanbae.globalcart.model.RequestInsertItem;
import com.hanbae.globalcart.model.ResponseAddFile;
import com.hanbae.globalcart.model.ResponseResult;
import com.hanbae.globalcart.util.PreferenceUtil;
import com.hanbae.globalcart.util.PrepareFilePart;
import com.hanbae.globalcart.view.fragment.home.SellMvpView;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Subscription;
import timber.log.Timber;

public class SellHistoryPresenter implements BasePresenter<SellMvpView> {

    private Activity mActivity;
    private SellMvpView sellMvpView;
    private Subscription mSubscription;
    private APIMultipartService mAPIMultipartService;
    private ResponseAddFile responseAddFile;
    private String returnString;

    private Uri mUploadFile;
    private String mModifySerialID;
//    private boolean isModify;
    private boolean isFileUpload;
    private boolean isCallToAddFile;

    @Inject
    public SellHistoryPresenter(Activity mActivity) {
        ((BaseApplication) mActivity.getApplicationContext()).getApplicationComponent().inject(this);
        this.mActivity = mActivity;
    }

    @Inject
    Gson gson;

    @Inject
    PreferenceUtil preferenceUtil;

    @Inject
    APIService apiService;

    @Override
    public void attachView(SellMvpView view) {
        sellMvpView = view;
        mAPIMultipartService = APIMultipartService.Factory.create(mActivity);

    }

    public Gson getGson (){
        return gson;
    }

    @Override
    public void detachView() {
        sellMvpView = null;
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    public void getPicture() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        mActivity.startActivityForResult(intent, 0);
    }

    public void insertItem(String name, String price, String newYN, String color, String comment, List<RequestInsertItem.ItemImageList> itemList){
        RequestInsertItem requestInsertItem = new RequestInsertItem();
        requestInsertItem.setItemName(name);
        if(!"".equals(price)){
            requestInsertItem.setItemPrice(Integer.parseInt(price));
        }
        requestInsertItem.setItemNewYN(newYN);
        requestInsertItem.setItemColor(color);
        requestInsertItem.setItemComment(comment);
        requestInsertItem.setItemImageList(itemList);
        requestInsertItem.setSellUserSerialID(preferenceUtil.getStringExtra("userSID"));

        Timber.e("requestInsertItem : "+ requestInsertItem);
            mSubscription = NetworkRequest.performAsyncRequest(apiService.insertItem(requestInsertItem), (data) -> {
            try {
                String returnString = new String(((Response<ResponseBody>) data).body().bytes());
                String decString = URLDecoder.decode(returnString, "UTF-8");
//                responseAddFile = new ResponseAddFile();
                ResponseResult responseResult = gson.fromJson(decString, ResponseResult.class);

                Timber.d("응답받은 값 : " + decString);
                Timber.d("응답받은 값(obj) : " + responseAddFile.toString());

                if(responseAddFile.getResult() != null) {
                    sellMvpView.addFile(APIService.IMAGE_URL+responseAddFile.getResult().getFileUploadResultVO().getFileName(), responseAddFile.getResult().getFileUploadResultVO().getFileSerialID(), responseAddFile.getResult().getFileUploadResultVO().getFileName());
                    isFileUpload = true;
                } else {
                    Toast.makeText(mActivity, mActivity.getString(R.string.upload_failed), Toast.LENGTH_SHORT).show();
                    isFileUpload = false;
                }
//                mReportMvpView.onDaysListDraw(mReportListHaribo);

                sellMvpView.cancelDialog();
                sellMvpView.resetView();
            } catch (Exception e) {
//                mHomeMvpView.hideDialog();
                Timber.e("Exception : " + e.getMessage());
                FirebaseCrash.report(e);
                sellMvpView.cancelDialog();
            }
        });
    }

//    public void addFile(Uri file) {
////        MultipartBody.Part body = PrepareFilePart.prepareFilePart(mActivity, "image", file);
//
//        if(isFileUpload) {
//            mUploadFile = file;
//            isCallToAddFile = true;
//            deleteFile();
//            return;
//        }
//
//        sellMvpView.showDialog(mActivity);
//
//        MultipartBody.Part body = PrepareFilePart.prepareFilePart(mActivity, "image", file);
//
//        if (mSubscription != null) mSubscription.unsubscribe();
//
//        mSubscription = NetworkRequest.performAsyncRequest(mAPIMultipartService.addFile(body), (data) -> {
//            try {
//                String returnString = new String(((Response<ResponseBody>) data).body().bytes());
//                String decString = URLDecoder.decode(returnString, "UTF-8");
//
////                mAddFileHaribo = gson.fromJson(decString, AddFileHaribo.class);
//
//                Timber.d("응답받은 값 : " + decString);
////                Timber.d("응답받은 값(obj) : " + mAddFileHaribo.toString());
//
//                if(mAddFileHaribo.getResult() != null) {
//                    if(isModify) {
//                        mModifySerialID = mAddFileHaribo.getResult().getFileSerialID();
//                    }
//                    sellMvpView.addFile(APIService.IMAGE_URL + mAddFileHaribo.getResult().getUrl(), mAddFileHaribo.getResult().getFileSerialID());
//                    isFileUpload = true;
//                } else {
//                    Toast.makeText(mActivity, mActivity.getString(R.string.diaryaddpresenter_picture_upload_failed), Toast.LENGTH_SHORT).show();
//                    isFileUpload = false;
//                }
//
//
////                mReportMvpView.onDaysListDraw(mReportListHaribo);
//
//                mDiaryAddMvpView.cancelDialog();
//            } catch (Exception e) {
////                mHomeMvpView.hideDialog();
//                Timber.e("Exception : " + e.getMessage());
//                FirebaseCrash.report(e);
//            }
//        });
//
//    }
//
//    public void deleteFile() {
//        if(!isFileUpload) {
//            return;
//        }
//
//        mDiaryAddMvpView.showDialog(mActivity);
//
//        Map map = new HashMap<String,String>();
//        Map map2 = new HashMap<String,String>();
//
//        if(isModify) {
//            map.put(mActivity.getString(R.string.api_params_deleteFile_fileSerialID), mModifySerialID);
//        } else {
//            map.put(mActivity.getString(R.string.api_params_deleteFile_fileSerialID), mAddFileHaribo.getResult().getFileSerialID());
//        }
//
//
//        if (mSubscription != null) mSubscription.unsubscribe();
//        map2.put("param",gson.toJson(map));
//        mSubscription = NetworkRequest.performAsyncRequest(mAPIService.deleteFile(map2), (data)->{
//            try {
//                String returnString = new String(((Response<ResponseBody>) data).body().bytes());
//                ResponseHaribo responseHaribo = gson.fromJson(returnString, ResponseHaribo.class);
//
//                Timber.d("응답받은 값 : " + returnString);
//                Timber.d("응답받은 값(obj) : " + responseHaribo.toString());
//
//                isFileUpload = false;
//                if(isCallToAddFile) {
//                    isCallToAddFile = false;
//                    addFile(mUploadFile);
//                } else {
//                    mDiaryAddMvpView.deleteFile();
//                }
//
////                mReportMvpView.onDaysListDraw(mReportListHaribo);
//
//                mDiaryAddMvpView.cancelDialog();
//            } catch (Exception e) {
////                mHomeMvpView.hideDialog();
//                Timber.e("Exception : " + e.getMessage());
//                FirebaseCrash.report(e);
//            }
//        });
//    }

}