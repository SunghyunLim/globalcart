package com.hanbae.globalcart.view.fragment.home;


import android.app.Activity;
import android.content.Context;
import android.provider.Settings;
import android.text.Editable;
import android.widget.Toast;

import com.bit.bitcare.baseapp.presenter.BasePresenter;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.hanbae.globalcart.BaseApplication;
import com.hanbae.globalcart.data.remote.APIService;
import com.hanbae.globalcart.data.remote.NetworkRequest;
import com.hanbae.globalcart.model.RequestInsertSearch;
import com.hanbae.globalcart.model.RequestRegist;
import com.hanbae.globalcart.model.ResponseRegist;
import com.hanbae.globalcart.model.ResponseResult;
import com.hanbae.globalcart.util.PreferenceUtil;
import com.hanbae.globalcart.view.activity.main.MainMvpView;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Subscription;
import timber.log.Timber;

public class BuyPresenter implements BasePresenter<BuyMvpView> {

    private Activity mActivity;
    private BuyMvpView buyMvpView;
    private Subscription mSubscription;

    private String returnString;


    @Inject
    public BuyPresenter(Activity mActivity) {
        ((BaseApplication) mActivity.getApplicationContext()).getApplicationComponent().inject(this);
        this.mActivity = mActivity;
    }

    @Inject
    Gson gson;

    @Inject
    PreferenceUtil preferenceUtil;

    @Inject
    APIService apiService;

    @Override
    public void attachView(BuyMvpView view) {
        buyMvpView = view;
    }

    public Gson getGson (){
        return gson;
    }

    @Override
    public void detachView() {
        buyMvpView = null;
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    //구매 물품 등록
    public void insertSearch(String searchText, String searchPrice, String searchNewYN) {
        RequestInsertSearch requestInsertSearch = new RequestInsertSearch();
        requestInsertSearch.setBuyUserSerialID(preferenceUtil.getStringExtra("userSID"));
        requestInsertSearch.setSearchText(searchText);
        requestInsertSearch.setSearchPrice(searchPrice);
        requestInsertSearch.setSearchNewYN(searchNewYN);

        Timber.e(requestInsertSearch.toString());

        if (mSubscription != null) mSubscription.unsubscribe();
        mSubscription = NetworkRequest.performAsyncRequest(apiService.insertSearch(requestInsertSearch), (data) -> {
            try {
                returnString = new String(((Response<ResponseBody>) data).body().bytes());
                Timber.e("returnString : " + returnString);
                ResponseResult response = gson.fromJson(returnString, ResponseResult.class);
                Timber.d("응답받은 값 : " + returnString);
                Timber.d("응답받은 값(obj) : " + response.toString());

                if("1".equals(response.getResult().getResultCode())){
                    Timber.e("구매 물품 등록 성공");
                    //여기 성공

                }else{
                    Toast.makeText(mActivity,"구매 물품 등록에 실패했습니다.", Toast.LENGTH_SHORT).show();
                }

                buyMvpView.resetView();
            } catch (Exception e) {
                Timber.e("Exception : " + e.getMessage());
                FirebaseCrash.report(e);
            }finally {

            }
        }, (error) -> {
        });
    }

}