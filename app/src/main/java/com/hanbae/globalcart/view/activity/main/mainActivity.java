package com.hanbae.globalcart.view.activity.main;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.hanbae.globalcart.R;
import com.hanbae.globalcart.model.ResponseLogin;
import com.hanbae.globalcart.util.BackButtonPressCloseHandler;
import com.hanbae.globalcart.view.activity.login.LoginActivity;
import com.hanbae.globalcart.view.activity.login.LoginMvpView;
import com.hanbae.globalcart.view.activity.login.LoginPresenter;
import com.hanbae.globalcart.view.base.AppBaseActivity;
import com.hanbae.globalcart.view.fragment.history.BuyHistoryFragment;
import com.hanbae.globalcart.view.fragment.history.SellHistoryFragment;
import com.hanbae.globalcart.view.fragment.home.BuyFragment;
import com.hanbae.globalcart.view.fragment.home.SellFragment;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

public class MainActivity extends AppBaseActivity implements MainMvpView, NavigationView.OnNavigationItemSelectedListener {

    private MainPresenter mainPresenter;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    private BackButtonPressCloseHandler backButtonPressCloseHandler;
    private InputMethodManager inputMethodManager;

//    @BindView(R.id.toolbar) Toolbar toolbar;
//    @BindView(R.id.drawer_layout) DrawerLayout drawer;
//    @BindView(R.id.nav_view) NavigationView navigationView;
//    @BindView(R.id.iv_account_profile) ImageView iv_account_profile;

    private DrawerLayout drawer;
    private TextView user_name;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Timber.e("requestCode : "+requestCode);
        Timber.e("resultCode : "+resultCode);
        if(resultCode != RESULT_OK) {
            return;
        }
        if("0".equals(String.valueOf(requestCode))||"1".equals(String.valueOf(requestCode))){
            String tag = getPreferenceUtil().getStringExtra("SellFragmentTag");
            Timber.e("onActivityResult fragment tag : " +tag);
            Fragment sellFragment = getSupportFragmentManager().findFragmentByTag(tag);
            sellFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(requestCode == SellFragment.MULTIPLE_PERMISSIONS) {
            String tag = getPreferenceUtil().getStringExtra("SellFragmentTag");
            Timber.e("onRequestPermissionsResult fragment tag : " +tag);
            Fragment sellFragment = getSupportFragmentManager().findFragmentByTag(tag);
            sellFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.e("MainActivity onCreate");
        inputMethodManager = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Drawer
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //키보드 내리기
//        inputMethodManager.hideSoftInputFromWindow(drawer.getWindowToken(), 0);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        ImageView iv_account_profile  = (ImageView) navigationView.findViewById(R.id.iv_account_profile);

        View header = navigationView.getHeaderView(0);
        user_name = (TextView) header.findViewById(R.id.user_name);
        String userInfo = getPreferenceUtil().getStringExtra("userInfo");
        Timber.e("userInfo : "+userInfo);
        ResponseLogin responseLogin = mainPresenter.getGson().fromJson(userInfo, ResponseLogin.class);
        user_name.setText(responseLogin.getResult().getUserName());

        //Viewpager
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_main;
    }


    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        mainPresenter = new MainPresenter(this);
        mainPresenter.attachView(this);

        backButtonPressCloseHandler = new BackButtonPressCloseHandler(this);


    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void hideKeyboard(){
//        inputMethodManager.hideSoftInputFromWindow(mTitle.getWindowToken(),0);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            backButtonPressCloseHandler.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action

        } else if (id == R.id.nav_list) {

        } else if (id == R.id.nav_person) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_notification) {

        } else if (id == R.id.nav_logout) {
            new MaterialDialog.Builder(this)
                    .title("로그아웃 하시겠습니까?")
                    .positiveText("네")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            Timber.e("로그아웃 버튼 눌림!");
                            getPreferenceUtil().putStringExtra("authVendor", "");
                            FirebaseAuth.getInstance().signOut();
                            enterActivity(LoginActivity.class);
                            finish();
                        }
                    })
                    .negativeText("아니오")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_home_buy, container, false);
//            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private Map<Integer, String> mFragmentTags;
        private FragmentManager mFragmentManager;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            mFragmentManager = fm;
            mFragmentTags = new HashMap<>();
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch(position){
                case 0:
                    return new BuyFragment();
                case 1:
                    return new SellFragment();
            }
            return null;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Object obj = super.instantiateItem(container, position);
            if (obj instanceof Fragment) {
                Fragment f = (Fragment) obj;
                String tag = f.getTag();
                Timber.e("in instatniatltem tag : "+ tag);
                if(position==1){
                    getPreferenceUtil().putStringExtra("SellFragmentTag", tag);
                }
                mFragmentTags.put(position, tag);
            }
            return obj;
        }

        public Fragment getFragment(int position) {
            String tag = mFragmentTags.get(position);
            if (tag == null)
                return null;
            return mFragmentManager.findFragmentByTag(tag);
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }
    }

    public class HistorySectionsPagerAdapter extends FragmentPagerAdapter {
        private Map<Integer, String> mFragmentTags;
        private FragmentManager mFragmentManager;

        public HistorySectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            mFragmentManager = fm;
            mFragmentTags = new HashMap<>();
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch(position){
                case 0:
                    return new BuyHistoryFragment();
                case 1:
                    return new SellHistoryFragment();
            }
            return null;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Object obj = super.instantiateItem(container, position);
            if (obj instanceof Fragment) {
                Fragment f = (Fragment) obj;
                String tag = f.getTag();
                Timber.e("in instatniatltem tag : "+ tag);
                if(position==1){
                    getPreferenceUtil().putStringExtra("SellFragmentTag", tag);
                }
                mFragmentTags.put(position, tag);
            }
            return obj;
        }

        public Fragment getFragment(int position) {
            String tag = mFragmentTags.get(position);
            if (tag == null)
                return null;
            return mFragmentManager.findFragmentByTag(tag);
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }
    }

}
