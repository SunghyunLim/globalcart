package com.hanbae.globalcart.view.fragment.history;

import com.hanbae.globalcart.view.base.AppBaseMvpView;

/**
 * Created by acid on 2018-04-24.
 */

public interface BuyHistoryMvpView extends AppBaseMvpView {
    public void resetView();
}
