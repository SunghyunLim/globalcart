package com.hanbae.globalcart.view.base;

import android.app.Activity;
import android.content.Context;

public interface AppBaseMvpView {

    Context getContext();
    void showDialog(Activity activity);
    void hideDialog();
    void cancelDialog();
    void dismissDialog();
}