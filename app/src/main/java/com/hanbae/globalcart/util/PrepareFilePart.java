package com.hanbae.globalcart.util;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import lombok.NonNull;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import timber.log.Timber;

/**
 * Created by yys on 2017-02-24.
 */

public class PrepareFilePart {
    public static String getRealPathFromURI(Context context, Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    public static String getRealPathFromURI2(Context context, Uri contentUri) {

        String[] filePath = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, filePath, null, null, null);
        cursor.moveToFirst();
        String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;


        Timber.d("size: " + ExifUtils.getExifImageSize(imagePath));

        if(ExifUtils.getExifImageSize(imagePath) > 600) {
            if(ExifUtils.getExifImageSize(imagePath) > 4000) {
                options.inSampleSize = 8;
            } else if(ExifUtils.getExifImageSize(imagePath) > 2000) {
                options.inSampleSize = 4;
            } else {
                options.inSampleSize = 2;
            }
        }

//        options.inSampleSize = 8;

        Timber.e("imagePath22 : "+imagePath);

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
        bitmap = ExifUtils.rotateBitmap(imagePath, bitmap);
        cursor.close();

        File file = new File("temp.png");
        FileOutputStream fos = null;
        try {
            fos = context.openFileOutput("temp.png" , 0);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100 , fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        bitmap.recycle();
        bitmap = null;

        return context.getFilesDir() + "/temp.png";

//        ByteArrayOutputStream stream = new ByteArrayOutputStream() ;
//        bitmap.compress( Bitmap.CompressFormat.PNG, 100, stream) ;
//        byte[] byteArray = stream.toByteArray() ;

//        String[] proj = { MediaStore.Images.Media.DATA };
//        CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
//        Cursor cursor = loader.loadInBackground();
////        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//        String imagePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
//        cursor.moveToFirst();
//
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
//        bitmap = ExifUtils.rotateBitmap(imagePath, bitmap);
////        String result = cursor.getString(column_index);
//        cursor.close();
//
//        ByteArrayOutputStream stream = new ByteArrayOutputStream() ;
//        bitmap.compress( Bitmap.CompressFormat.PNG, 100, stream) ;
//        byte[] byteArray = stream.toByteArray() ;


//        return "";
    }

    public static String getRealPathFromURI3(Context context, Uri contentUri) {

        String imagePath = Environment.getExternalStorageDirectory() + contentUri.getPath();

        Timber.d("contentUri.getPath: " + contentUri.getPath());

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;


        Timber.d("size: " + ExifUtils.getExifImageSize(imagePath));

        if (ExifUtils.getExifImageSize(imagePath) > 600) {
            if (ExifUtils.getExifImageSize(imagePath) > 4000) {
                options.inSampleSize = 8;
            } else if (ExifUtils.getExifImageSize(imagePath) > 2000) {
                options.inSampleSize = 4;
            } else {
                options.inSampleSize = 2;
            }
        }

        Timber.e("imagePath33 : "+imagePath);

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
        bitmap = ExifUtils.rotateBitmap(imagePath, bitmap);

        File file = new File("temp.png");
        FileOutputStream fos = null;
        try {
            fos = context.openFileOutput("temp.png", 0);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        bitmap.recycle();
        bitmap = null;

        return context.getFilesDir() + "/temp.png";
    }

    @NonNull
    public static MultipartBody.Part prepareFilePart(Context context, String partName, Uri fileUri, int type) {
        File file;

        if(type==0){
            Timber.e("22222: " + getRealPathFromURI2(context, fileUri));
            file = new File(getRealPathFromURI2(context, fileUri));
        }else{
            Timber.e("33333: " + getRealPathFromURI3(context, fileUri));
            file = new File(getRealPathFromURI3(context, fileUri));
        }

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/png"), file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }
}
