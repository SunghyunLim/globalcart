package com.hanbae.globalcart.util;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Build;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by yys on 2017-03-09.
 */

public class ExifUtils {
    public static Bitmap rotateBitmap(String src, Bitmap bitmap) {
        try {
            int orientation = getExifOrientation(src);
            if (orientation == 1) {
                return bitmap;
            }
            Matrix matrix = new Matrix();
            switch (orientation) {
                case 2:
                    matrix.setScale(-1, 1);
                    break;
                case 3:
                    matrix.setRotate(180);
                    break;
                case 4:
                    matrix.setRotate(180);
                    matrix.postScale(-1, 1);
                    break;
                case 5:
                    matrix.setRotate(90);
                    matrix.postScale(-1, 1);
                    break;
                case 6:
                    matrix.setRotate(90);
                    break;
                case 7:
                    matrix.setRotate(-90);
                    matrix.postScale(-1, 1);
                    break;
                case 8:
                    matrix.setRotate(-90);
                    break;
                default:
                    return bitmap;
            }
            try {
                Bitmap oriented = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                bitmap.recycle();
                return oriented;
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                return bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static int getExifImageSize(String src) {
        int orientation = 1;
        int width = 0;
        int height = 0;
        Class<?> exifClass = null;
        try {
            exifClass = Class.forName("android.media.ExifInterface");
            Constructor<?> exifConstructor = exifClass.getConstructor(new Class[]{String.class});
            Object exifInstance = exifConstructor.newInstance(new Object[]{src});
            Method getAttributeInt = exifClass.getMethod("getAttributeInt", new Class[]{String.class, int.class});
            Field tagIamgeWidth = exifClass.getField("TAG_IMAGE_WIDTH");
            Field tagIamgeHeight = exifClass.getField("TAG_IMAGE_LENGTH");
            Field tagOrientationField = exifClass.getField("TAG_ORIENTATION");
            String tagOrientation = (String) tagOrientationField.get(null);
            orientation = (Integer) getAttributeInt.invoke(exifInstance, new Object[]{tagOrientation, 1});

            String tagWidth = (String) tagIamgeWidth.get(null);
            String tagHeight = (String) tagIamgeHeight.get(null);
            width = (Integer) getAttributeInt.invoke(exifInstance, new Object[]{tagWidth, 1});
            height = (Integer) getAttributeInt.invoke(exifInstance, new Object[]{tagHeight, 1});
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        if(orientation == ExifInterface.ORIENTATION_ROTATE_90 || orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return height;
        } else {
            return width;
        }

//        return Math.max(width, height);
    }

    private static int getExifOrientation(String src) throws IOException {
        int orientation = 1;
        try { /** * if your are targeting only api level >= 5 ExifInterface exif = * new ExifInterface(src); orientation = * exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1); */
            if (Build.VERSION.SDK_INT >= 5) {
                Class<?> exifClass = Class.forName("android.media.ExifInterface");
                Constructor<?> exifConstructor = exifClass.getConstructor(new Class[]{String.class});
                Object exifInstance = exifConstructor.newInstance(new Object[]{src});
                Method getAttributeInt = exifClass.getMethod("getAttributeInt", new Class[]{String.class, int.class});
                Field tagOrientationField = exifClass.getField("TAG_ORIENTATION");
                String tagOrientation = (String) tagOrientationField.get(null);
                orientation = (Integer) getAttributeInt.invoke(exifInstance, new Object[]{tagOrientation, 1});
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return orientation;
    }

    public static String getExifCompression(String src) throws IOException {
        String fileSource = null;
        try { /** * if your are targeting only api level >= 5 ExifInterface exif = * new ExifInterface(src); orientation = * exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1); */
            if (Build.VERSION.SDK_INT >= 5) {
                Class<?> exifClass = Class.forName("android.media.ExifInterface");
                Constructor<?> exifConstructor = exifClass.getConstructor(new Class[]{String.class});
                Object exifInstance = exifConstructor.newInstance(new Object[]{src});
                Method getAttribute = exifClass.getMethod("getAttribute", new Class[]{String.class, int.class});
                Field tagOrientationField = exifClass.getField(ExifInterface.TAG_FILE_SOURCE);
                String tagOrientation = (String) tagOrientationField.get(null);
                fileSource = (String) getAttribute.invoke(exifInstance, new Object[]{tagOrientation, 1});



            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return fileSource;
    }

}
