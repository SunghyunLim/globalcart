package com.hanbae.globalcart.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by chaoziliang on 16/1/13.
 */
public class DateUtil {

    public static final String			DATE_FORMAT_YYYYMMDDHHMMSS 		= "yyyy-MM-dd hh:mm:ss";
    public static final String			DATE_FORMAT_YYYYMMDDHHMMSS2 	= "yyyyMMddhhmmss";
    public static final String			DATE_FORMAT_YYYYMMDDHHMMSS3 	= "yyyyMMddHHmmss";

    public static final String			DATE_FORMAT_YYYYMMDDHHMM 		= "yyyy-MM-dd HH:mm";
    public static final String			DATE_FORMAT_YYYYMMDDHHMM2 		= "yyyyMMddHHmm";
    public static final String			DATE_FORMAT_YYYYMMDDHHMM3 		= "yyyy년 MM월 dd일\nHH시 mm분";

    /**
     * 날짜 구하기
     *
     * @return 오늘날짜 yyyy-MM-dd
     */
    public static String getStringDateShort() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(currentTime);
    }

    public static String getStringDateLong() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        return formatter.format(currentTime);
    }

    /**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd HH:mm:ss
     *
     * @param strDate
     * @return
     */
    public static Date strToDateLong(String strDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ParsePosition pos = new ParsePosition(0);
        Date strtodate = formatter.parse(strDate, pos);
        return strtodate;
    }

    public static String toChangeTimeFormat(String strDate){
        String result = StringToDate(strDate, "yyyy-MM-dd", "MM-dd");

        return result;
    }

    public static String toChangeDateFormat(String strDate){
        String result = StringToDate(strDate, "yyyyMMdd", "yyyy-MM-dd");

        return result;
    }

    public static String StringToDate(String dateStr, String dateFormatStr, String formatStr) {
        DateFormat sdf = new SimpleDateFormat(dateFormatStr);
        Date date = null;
        try{
            date = sdf.parse(dateStr);

        } catch (ParseException e){
            e.printStackTrace();
        }
        SimpleDateFormat s = new SimpleDateFormat(formatStr);

        return s.format(date);
    }

    /**
     * 将长时间格式时间转换为字符串 yyyy-MM-dd HH:mm:ss  *   * @param dateDate  * @return
     */
    public static String dateToStrLong(Date dateDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(dateDate);
        return dateString;
    }

    /**
     * 将短时间格式时间转换为字符串 yyyy-MM-dd
     *
     * @param dateDate
     * @param
     * @return
     */
    public static String dateToStr(Date dateDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(dateDate);
        return dateString;
    }

    /**
     * 将短时间格式字符串转换为时间 yyyy-MM-dd
     *
     * @param strDate
     * @return
     */
    public static Date strToDate(String strDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        ParsePosition pos = new ParsePosition(0);
        Date strtodate = formatter.parse(strDate, pos);

        return strtodate;
    }

    /**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd HH:mm:ss
     *
     * @param
     * @return
     */
    public static String getStrFromTime(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//
        return formatter.format(time);
    }

    public static String getStrFromTime14(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");//
        return formatter.format(time);
    }

    public static String getStrFromTime8(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");//
        return formatter.format(time);
    }


    public static boolean getBooleanMidnightCompare(long time) {

        SimpleDateFormat formatter = new SimpleDateFormat("HHmm");
        String dateString = formatter.format(time);

        return !dateString.equals("2359");
    }

    /**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd
     *
     * @param
     * @return
     */
    public static String getStrOtherFromTime(long time) {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");//初始化Formatter的转换格式。
        String dateString = formatter.format(time);

        return dateString;
    }

    public static String getStrYesterDay(long time) {

        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, -1);
        Date date = cal.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);

        return dateString;
    }

    public static String getStrYesterDayyyyyMMdd(long time) {

        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, -1);
        Date date = cal.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String dateString = formatter.format(date);

        return dateString;
    }


    /**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd
     *
     * @param
     * @return
     */
    public static String getHoursFromTime(long time) {

        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");//初始化Formatter的转换格式。
        String dateString = formatter.format(time);

        return dateString;
    }

    public static long getTimeLongType(String strDate) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        Date date = (Date)formatter.parse(strDate);
        return date.getTime();
    }

    /**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd HH:mm
     *
     * @param
     * @return
     */
    public static String getYearDateFromTime(long time) {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//初始化Formatter的转换格式。
        String dateString = formatter.format(time);

        return dateString;
    }

    public static String getTotalTime(Integer time) {
        long sportTime = time - TimeZone.getDefault().getRawOffset();
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");//初始化Formatter的转换格式。
        String hms = formatter.format(sportTime);
        return hms;
    }

    //获取时分秒分隔值
    public static String[] getTotalTimeDivide(Integer time) {
        String[] timeDivides = new String[3];
        long sportTime = time - TimeZone.getDefault().getRawOffset();
        SimpleDateFormat formatter = new SimpleDateFormat("HH");//初始化Formatter的转换格式。
        String hms = formatter.format(sportTime);
        timeDivides[0] = hms;
        formatter = new SimpleDateFormat("mm");
        String hms2 = formatter.format(sportTime);
        timeDivides[1] = hms2;
        formatter = new SimpleDateFormat("ss");
        String hms3 = formatter.format(sportTime);
        timeDivides[2] = hms3;
        return timeDivides;
    }

    //获取两位数的字符串
    public static String getStringFromDouble(double value){
        return String.format("%.02f", value);
    }

    //获取1位数的字符串
    public static String getOneStringFromDouble(double value){

        return String.format("%.01f", value);
    }

    /**
     * 小数点处理，保留一个小数点
     */
    public static float decimalProcess(double data) {
        return Math.round(data * 10.0f) / 10.0f;
    }


    // 通过毫秒获取 获取是日期 4/15
    public static String getDay(Long endTime){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(endTime);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH)+1;
        return month+"/"+day;
    }

    /**
     * dateformat변경
     * @param date
     * @param old_format
     * @param new_format
     * @return
     */
    public static String getNewDateFormat(String date, String old_format, String new_format) {
        SimpleDateFormat _old = new SimpleDateFormat(old_format);
        SimpleDateFormat _new = new SimpleDateFormat(new_format);

        Date ori_date;
        String new_date = null;
        try {
            ori_date = _old.parse(date);
            new_date = _new.format(ori_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new_date;
    }

    public static int[] calculateRemainMinute(){
        int hour = 0;
        int minute = 0;
        Date date = new Date();
        int[] returnArry = new int[2];

        SimpleDateFormat sdfHour = new SimpleDateFormat("HH", Locale.KOREA);
        String hourString = sdfHour.format(date);
        SimpleDateFormat sdfMinute = new SimpleDateFormat("mm", Locale.KOREA);
        String minuteString = sdfMinute.format(date);

        hour = Integer.parseInt(hourString);
        minute = Integer.parseInt(minuteString);

        int remainder = 0;
        if(minute!=0){
            if(minute>30){
                remainder = minute % 30;
            }else{

            }

        }else{
            returnArry[0]=hour;
            returnArry[1]=0;
            return returnArry;
        }
        returnArry[0]=hour;
        returnArry[1]=minute;
        return returnArry;
    }

    public static int calculateRemainHour(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH");
        String dateString = sdf.format(date);

        return Integer.parseInt(dateString);
    }



}
