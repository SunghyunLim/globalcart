package com.hanbae.globalcart.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import retrofit2.adapter.rxjava.HttpException;

public class NetworkUtil {

    public static boolean isHttpStatusCode(Throwable throwable, int statusCode) {
        return throwable instanceof HttpException && ((HttpException) throwable).code() == statusCode;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = null;
        NetworkInfo activeNetwork = null;
        for(int i=0; i < 10 ;i++) {
            cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            activeNetwork = cm.getActiveNetworkInfo();
            if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()){
                break;
            }else{
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    //지금은 안쓰지만 예전에 쓰던거... 네트워크 상태 확인
    public static boolean getInternetState(Context context){

        boolean availableInternet = false;

        NetworkInfo is3g = null;
        NetworkInfo isWifi = null;
        NetworkInfo is4g = null;

        try{
            // 3G를 사용하는지 확인한다.
            is3g = ((ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE)).getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            // WIFI를 사용하는지 확인한다.
            isWifi = ((ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE)).getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            // 4G를 사용하는지 확인한다.
            is4g = ((ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE)).getNetworkInfo(ConnectivityManager.TYPE_WIMAX);

        }catch(Exception e){

        }

        if (is4g != null){
            availableInternet = is4g.isConnected();
        }

        if (is3g != null) {
            if (is3g.isConnected() || isWifi.isConnected() || availableInternet){
                availableInternet = true;
            }

        } else {
            if(isWifi != null){
                if(isWifi.isConnected() || availableInternet){
                    availableInternet = true;
                }
            }
        }

        return availableInternet;
    }
}