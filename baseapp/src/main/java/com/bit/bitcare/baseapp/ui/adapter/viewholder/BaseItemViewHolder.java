package com.bit.bitcare.baseapp.ui.adapter.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

import static com.bit.bitcare.baseapp.ui.adapter.BaseRecyclerAdapter.OnItemClickListener;
import static com.bit.bitcare.baseapp.ui.adapter.BaseRecyclerAdapter.OnLongItemClickListener;
/**
 * Created by acid on 2016-11-01.
 */

public abstract class BaseItemViewHolder<Data> extends RecyclerView.ViewHolder implements
        View.OnClickListener,
        View.OnLongClickListener {
    protected Context mContext;
    private OnItemClickListener mItemClickListener;
    private OnLongItemClickListener mLongItemClickListener;
    private boolean mHasHeader = false;
    private Unbinder unbinder;


    public BaseItemViewHolder(View itemView, OnItemClickListener itemClickListener, OnLongItemClickListener longItemClickListener) {
        super(itemView);
        unbinder = ButterKnife.bind(this, itemView);
        Timber.tag(getClass().getSimpleName());
        this.mItemClickListener = itemClickListener;
        this.mLongItemClickListener = longItemClickListener;
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    public abstract void bind(Data data);

    public boolean isHasHeader() { return mHasHeader; }

    public void setHasHeader(boolean hasHeader) {
        this.mHasHeader = hasHeader;
    }

    @Override
    public void onClick(View v) {
        if (mItemClickListener != null) {
            mItemClickListener.onItemClick(v, mHasHeader ? getAdapterPosition() - 1 : getAdapterPosition());
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mLongItemClickListener != null) {
            mLongItemClickListener.onLongItemClick(v, mHasHeader ? getAdapterPosition() - 1 : getAdapterPosition());
            return true;
        }
        return false;
    }


}
